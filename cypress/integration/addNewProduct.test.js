describe("Pridėti naują produktą testas", 
    () => {
        it("Pridėti naują produktą", () => {
            cy.visit("/login")
            cy.get(':nth-child(1) > div > .form-control').clear();
            cy.get(':nth-child(1) > div > .form-control').type('Admin@email.com');
            cy.get(':nth-child(2) > div > .form-control').clear();
            cy.get(':nth-child(2) > div > .form-control').type('Admin');
            cy.get('.btn-m').click();
            cy.get('#collasible-nav-dropdown').click();
            cy.get('#addProduct').click();
            cy.get(':nth-child(1) > .form-control').clear();
            cy.get(':nth-child(1) > .form-control').type('PRODUCT777');
            cy.get(':nth-child(2) > .form-control').clear();
            cy.get(':nth-child(2) > .form-control').type('PRODUCT777');
            cy.get(':nth-child(3) > .form-control').clear();
            cy.get(':nth-child(3) > .form-control').type('300');
            cy.get(':nth-child(4) > .form-control').clear();
            cy.get(':nth-child(4) > .form-control').type('PRODUCT777');
            cy.get(':nth-child(5) > input').clear();
            cy.get(':nth-child(5) > input').type('200€');
            cy.get('.btn').click();
        })
    }
)