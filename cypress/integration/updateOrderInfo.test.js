describe("Atnaujinti užsakymo informaciją testas", 
    () => {
        it("Atnaujinti užsakymo informaciją", () => {
            cy.visit("/login")
            cy.get(':nth-child(1) > div > .form-control').clear();
            cy.get(':nth-child(1) > div > .form-control').type('Admin@email.com');
            cy.get(':nth-child(2) > div > .form-control').clear();
            cy.get(':nth-child(2) > div > .form-control').type('Admin');
            cy.get('.btn-m').click();
            cy.get('#collasible-nav-dropdown\\ Užsakymai').click();
            cy.get('#orders').click();
            cy.get('tbody > :nth-child(2) > :nth-child(4)').click();
            cy.get(':nth-child(2) > :nth-child(4) > svg').click();
            cy.get('#root > div.container-fluid > div:nth-child(5) > div > div > div > div:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(3)').click();
            cy.get('#root > div.container-fluid > div:nth-child(5) > div > div > div > div:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(3)').clear();
            cy.get('#root > div.container-fluid > div:nth-child(5) > div > div > div > div:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(3)').type('1333');
            cy.get(':nth-child(1) > :nth-child(4) > svg').click();
        })
    }
)