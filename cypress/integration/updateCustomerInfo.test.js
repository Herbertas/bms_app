describe("Atnaujinti kliento duomenis testas", 
    () => {
        it("Atnaujinti kliento duomenis", () => {
            cy.visit("/login")
            cy.get(':nth-child(1) > div > .form-control').clear();
            cy.get(':nth-child(1) > div > .form-control').type('Admin@email.com');
            cy.get(':nth-child(2) > div > .form-control').clear();
            cy.get(':nth-child(2) > div > .form-control').type('Admin');
            cy.get('.btn-m > span').click();
            cy.get('#clients').click();
            cy.get(':nth-child(1) > :nth-child(8) > svg').click();
            cy.get('#root > div.container-fluid > div:nth-child(5) > div > div > div > div:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(7)').click();
            cy.get('#root > div.container-fluid > div:nth-child(5) > div > div > div > div:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(7)').clear();
            cy.get('#root > div.container-fluid > div:nth-child(5) > div > div > div > div:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(7)').type('Rusija');
            cy.get(':nth-child(1) > :nth-child(8) > svg').click();
        })
    }
)