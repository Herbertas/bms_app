describe("Pridėti produkto klaidą testas", 
    () => {
        it("Pridėti produkto klaidą", () => {
            cy.visit("/login")
            cy.get(':nth-child(1) > div > .form-control').clear();
            cy.get(':nth-child(1) > div > .form-control').type('Admin@email.com');
            cy.get(':nth-child(2) > div > .form-control').clear();
            cy.get(':nth-child(2) > div > .form-control').type('Admin');
            cy.get('.btn-m').click();
            cy.get('#collasible-nav-dropdown').click();
            cy.get('#addProductFault').click();
            cy.get(':nth-child(1) > .css-2b097c-container > .css-yk16xz-control > .css-g1d714-ValueContainer').click();
            cy.get('#react-select-5-option-0').click();
            cy.get(':nth-child(2) > .css-2b097c-container > .css-yk16xz-control > .css-g1d714-ValueContainer').click();
            cy.get('#react-select-6-option-0').click();
            cy.get('.css-1wa3eu0-placeholder').click();
            cy.get('#react-select-7-option-0').click();
            cy.get('.form-control').click();
            cy.get('.form-control').clear();
            cy.get('.form-control').type('Aprašymas');
            cy.get('.btn').click();
        })
    }
)