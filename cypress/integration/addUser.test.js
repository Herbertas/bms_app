describe("Pridėti naują naudotoją", 
    () => {
        it("Pridėti naują naudotoją", () => {
            cy.visit("/login")
            cy.get(':nth-child(1) > div > .form-control').clear();
            cy.get(':nth-child(1) > div > .form-control').type('Admin@email.cim');
            cy.get(':nth-child(1) > div > .form-control').clear();
            cy.get(':nth-child(1) > div > .form-control').type('Admin@email.com');
            cy.get(':nth-child(2) > div > .form-control').clear();
            cy.get(':nth-child(2) > div > .form-control').type('Admin');
            cy.get('.btn-m > span').click();
            cy.get('#users').click();
            cy.get('.container-fluid > :nth-child(1)').click();
            cy.get('.btn-m').click();
            cy.get(':nth-child(1) > .form-control').clear();
            cy.get(':nth-child(1) > .form-control').type('TestoNaudotojas');
            cy.get(':nth-child(2) > .form-control').clear();
            cy.get(':nth-child(2) > .form-control').type('TestoNaudotojas');
            cy.get(':nth-child(3) > .form-control').clear();
            cy.get(':nth-child(3) > .form-control').type('TestoNaudotojas@email.com');
            cy.get(':nth-child(4) > .form-control').clear();
            cy.get(':nth-child(4) > .form-control').type('TestoNaudotojas');
            cy.get('.css-2b097c-container').click();
            cy.get('#react-select-2-option-0').click();
            cy.get('.react-datepicker__input-container > input').click();
            cy.get('.react-datepicker__day--007').click();
            cy.get(':nth-child(7) > .form-control').clear();
            cy.get(':nth-child(7) > .form-control').type('TestoNaudotojas g. 22');
            cy.get(':nth-child(8) > .form-control').clear();
            cy.get(':nth-child(8) > .form-control').type('868622908');
            cy.get('.btn').click();
        })
    }
)