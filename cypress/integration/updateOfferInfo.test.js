describe("Atnaujinti pasiūlymo duomenis testas", 
    () => {
        it("Atnaujinti pasiūlymo duomenis", () => {
            cy.visit("/login")
            cy.get(':nth-child(1) > div > .form-control').clear();
            cy.get(':nth-child(1) > div > .form-control').type('Admin@email.com');
            cy.get(':nth-child(2) > div > .form-control').clear();
            cy.get(':nth-child(2) > div > .form-control').type('Admin');
            cy.get('.btn-m > span').click();
            cy.get('#collasible-nav-dropdown\\ Užsakymai').click();
            cy.get('#offers').click();
            cy.get(':nth-child(1) > :nth-child(8) > svg').click();
            cy.get('#root > div.container-fluid > div:nth-child(5) > div > div > div > div:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(7)').click();
            cy.get('#root > div.container-fluid > div:nth-child(5) > div > div > div > div:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(7)').clear();
            cy.get('#root > div.container-fluid > div:nth-child(5) > div > div > div > div:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(7)').type('UAB Rampa');
            cy.get(':nth-child(1) > :nth-child(8) > svg').click();
        })
    }
)