describe("Pridėti užsakymą", 
    () => {
        it("Pridėti užsakymą", () => {
            cy.visit("/login")
            cy.get('form').click();
            cy.get(':nth-child(1) > div > .form-control').clear();
            cy.get(':nth-child(1) > div > .form-control').type('Admin@email.com');
            cy.get(':nth-child(2) > div > .form-control').clear();
            cy.get(':nth-child(2) > div > .form-control').type('Admin');
            cy.get('.btn-m > span').click();
            cy.get('#collasible-nav-dropdown\\ Užsakymai').click();
            cy.get('#addNewOrder').click();
            cy.get('.css-g1d714-ValueContainer').click();
            cy.get('#react-select-3-option-0').click();
            cy.get('#root > div.container-fluid > form > div:nth-child(2) > div > div > div > div:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(7) > svg').click();
            cy.get('.btn').click();
            cy.get(':nth-child(2) > .react-datepicker-wrapper > .react-datepicker__input-container > input').click();
            cy.get('.react-datepicker__day--007').click();
            cy.get(':nth-child(3) > .react-datepicker-wrapper > .react-datepicker__input-container > input').click();
            cy.get('.react-datepicker__day--008').click();
            cy.get('.css-1wa3eu0-placeholder').click();
            cy.get('#react-select-5-option-0').click();
            cy.get(':nth-child(7) > .react-datepicker-wrapper > .react-datepicker__input-container > input').click();
            cy.get('.react-datepicker__day--007').click();
            cy.get('#Svoris').clear();
            cy.get('#Svoris').type('300g');
            cy.get('#Tūris').clear();
            cy.get('#Tūris').type('1,000cm³');
            cy.get(':nth-child(14) > .form-control').clear();
            cy.get(':nth-child(14) > .form-control').type('vienuolika euru');
            cy.get(':nth-child(15) > input').clear();
            cy.get(':nth-child(15) > input').type('22%');
            cy.get(':nth-child(16) > .form-control').clear();
            cy.get(':nth-child(16) > .form-control').type('trylika');
            cy.get(':nth-child(17) > .react-datepicker-wrapper > .react-datepicker__input-container > input').click();
            cy.get('.react-datepicker__day--007').click();
            cy.get(':nth-child(18) > .react-datepicker-wrapper > .react-datepicker__input-container > input').click();
            cy.get('.react-datepicker__day--008').click();
            cy.get('.btn').click();
        })
    }
)