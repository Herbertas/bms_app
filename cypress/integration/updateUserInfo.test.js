describe("Redaguoti naudotojo duomenis", 
    () => {
        it("Redaguoti naudotojo duomenis", () => {
            cy.visit("/login")
            cy.get(':nth-child(1) > div > .form-control').clear();
            cy.get(':nth-child(1) > div > .form-control').type('Admin@email.com');
            cy.get(':nth-child(2) > div > .form-control').clear();
            cy.get(':nth-child(2) > div > .form-control').type('Admin');
            cy.get('.btn-m').click();
            cy.get('#users').click();
            cy.get(':nth-child(1) > :nth-child(6) > svg').click();
            cy.get('#root > div.container-fluid > div > div:nth-child(5) > div > div > div > div:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(5)').click();
            cy.get('#root > div.container-fluid > div > div:nth-child(5) > div > div > div > div:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(5)').clear();
            cy.get('#root > div.container-fluid > div > div:nth-child(5) > div > div > div > div:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(5)').type('Vilnius, Antakalnio g. 22');
            cy.get(':nth-child(1) > :nth-child(6) > svg').click();
        })
    }
)