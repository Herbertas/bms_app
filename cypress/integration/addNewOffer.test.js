describe("Pridėti naują pasiūlymą testas", 
    () => {
        it("Pridėti naują pasiūlymą", () => {
            cy.visit("/login")
            cy.get(':nth-child(1) > div > .form-control').clear();
            cy.get(':nth-child(1) > div > .form-control').type('Admin@email.com');
            cy.get(':nth-child(2) > div > .form-control').clear();
            cy.get(':nth-child(2) > div > .form-control').type('Admin{enter}');
            cy.get('#collasible-nav-dropdown\\ Užsakymai').click();
            cy.get('#addNewOffer').click();
            cy.get(':nth-child(1) > .css-2b097c-container > .css-yk16xz-control > .css-g1d714-ValueContainer').click();
            cy.get('#react-select-4-option-0').click();
            cy.get('.css-yk16xz-control > .css-g1d714-ValueContainer').click();
            cy.get('#react-select-5-option-0').click();
            cy.get('.form-control').clear();
            cy.get('.form-control').type('1000');
            cy.get(':nth-child(4) > input').clear();
            cy.get(':nth-child(4) > input').type('11.22€');
            cy.get(':nth-child(5) > .react-datepicker-wrapper > .react-datepicker__input-container > input').click();
            cy.get('.react-datepicker__day--007').click();
            cy.get(':nth-child(6) > .react-datepicker-wrapper > .react-datepicker__input-container > input').click();
            cy.get('.react-datepicker__day--008').click();
            cy.get('.btn').click();
        })
    }
)