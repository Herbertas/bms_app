describe("Pridėti naują klientą testas", 
    () => {
        it("pridėti naują klientą", () => {
            cy.visit("/login")
            cy.get(':nth-child(1) > div > .form-control').clear();
            cy.get(':nth-child(1) > div > .form-control').type('Admin@email.com');
            cy.get(':nth-child(2) > div > .form-control').clear();
            cy.get(':nth-child(2) > div > .form-control').type('Admin');
            cy.get('.btn-m > span').click();
            cy.get('#clients').click();
            cy.get('.btn-m').click();
            cy.get(':nth-child(1) > .css-2b097c-container > .css-yk16xz-control > .css-g1d714-ValueContainer > .css-1wa3eu0-placeholder').click();
            cy.get('#react-select-2-option-0').click();
            cy.get('.css-1wa3eu0-placeholder').click();
            cy.get('#react-select-3-option-0').click();
            cy.get(':nth-child(3) > .form-control').clear();
            cy.get(':nth-child(3) > .form-control').type('123456789');
            cy.get(':nth-child(4) > .form-control').clear();
            cy.get(':nth-child(4) > .form-control').type('"UAB SVEIKATA"');
            cy.get(':nth-child(5) > .form-control').click();
            cy.get(':nth-child(4) > .form-control').click();
            cy.get(':nth-child(5) > .form-control').clear();
            cy.get(':nth-child(5) > .form-control').type('Sveikatos g.33');
            cy.get(':nth-child(6) > .form-control').clear();
            cy.get(':nth-child(6) > .form-control').type('123456789');
            cy.get(':nth-child(7) > .form-control').clear();
            cy.get(':nth-child(7) > .form-control').type('123456789');
            cy.get(':nth-child(3) > .form-control').clear();
            cy.get(':nth-child(3) > .form-control').type('123456111');
            cy.get(':nth-child(6) > .form-control').clear();
            cy.get(':nth-child(6) > .form-control').type('123456111');
            cy.get(':nth-child(7) > .form-control').clear();
            cy.get(':nth-child(7) > .form-control').type('123456111');
            cy.get('form > :nth-child(8)').click();
            cy.get(':nth-child(8) > .form-control').clear();
            cy.get(':nth-child(8) > .form-control').type('86833446');
            cy.get(':nth-child(9) > .form-control').clear();
            cy.get(':nth-child(9) > .form-control').type('Sveikata@email.com');
            cy.get(':nth-child(5) > .form-control').clear();
            cy.get(':nth-child(5) > .form-control').type('Sveikatos g.33');
            cy.get(':nth-child(10) > .form-control').clear();
            cy.get(':nth-child(10) > .form-control').type('Sveikatos g.33');
            cy.get(':nth-child(11) > .form-control').clear();
            cy.get(':nth-child(11) > .form-control').type('Sveikas');
            cy.get(':nth-child(12) > .form-control').clear();
            cy.get(':nth-child(12) > .form-control').type('868622317');
            cy.get('.btn').click();
        })
    }
)