describe("Atnaujinti produkto likutį sandėlyje testas", 
    () => {
        it("Atnaujinti produkto likutį", () => {
            cy.visit("/login")
            cy.get(':nth-child(1) > div > .form-control').clear();
            cy.get(':nth-child(1) > div > .form-control').type('Admin@email.com');
            cy.get(':nth-child(2) > div > .form-control').clear();
            cy.get(':nth-child(2) > div > .form-control').type('Admin');
            cy.get('.btn-m').click();
            cy.get('#storage').click();
            cy.get('[style="border-bottom: 1px solid rgb(224, 224, 224); color: rgba(0, 0, 0, 0.87); height: 48px;"] > :nth-child(7) > svg').click();
            cy.get('#root > div.container-fluid > div:nth-child(6) > div > div > div > div:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(3)').click();
            cy.get('#root > div.container-fluid > div:nth-child(6) > div > div > div > div:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(3)').clear();
            cy.get('#root > div.container-fluid > div:nth-child(6) > div > div > div > div:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(3)').type('40');
            cy.get('[style="border-bottom: 1px solid rgb(224, 224, 224); color: rgba(0, 0, 0, 0.87); height: 48px;"] > :nth-child(7) > svg').click();
        })
    }
)