import React from "react";
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn
} from "material-ui/Table";
import EditIcon from "material-ui/svg-icons/image/edit";
import TrashIcon from "material-ui/svg-icons/action/delete";
import CheckIcon from "material-ui/svg-icons/navigation/check";
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxRoundedIcon from '@material-ui/icons/CheckBoxRounded';
import TextField from "material-ui/TextField";
import { Paper } from "@material-ui/core";

const row = (
  x,
  i,
  header,
  handleRemove,
  startEditing,
  editIdx,
  handleChange,
  stopEditing, 
  selectable,
) => {
  const currentlyEditing = editIdx === i
  return (
    <TableRow key={`tr-${i}`} selectable={(selectable == null) ? false : true}>
      {header.map((y, k) => (
        <TableRowColumn key={`trc-${k}`}>
          {currentlyEditing && handleChange != null ? (
            <TextField
              name={y.prop}
              onChange={e => handleChange(e, y.prop, i)}
              value={x[y.prop]}
            />
          ) : (
            x[y.prop]
          )}
        </TableRowColumn>
      ))}
      <TableRowColumn>
        {currentlyEditing ? (
          (selectable == null) ?
          <CheckIcon onClick={() => stopEditing()} />
          :
          <CheckBoxRoundedIcon onClick={() => stopEditing()} />
        ) : (
          (selectable == null) ?
          <EditIcon onClick={() => startEditing(i)} />
          : 
          <CheckBoxOutlineBlankIcon onClick={() => startEditing(i)} />
        )}
      </TableRowColumn>
      {(handleRemove != null) ? 
      <TableRowColumn>
        <TrashIcon onClick={() => handleRemove(i)} />
      </TableRowColumn>
      : null
    }
    </TableRow>
  );
};

export default ({
  data,
  header,
  handleRemove,
  startEditing,
  editIdx,
  handleChange,
  stopEditing, 
  selectable
}) => (
  <Paper classname="container" style={{backgroundColor: "#efefef"}}>
    <Table style={{width: "100%", display:'box', overflowX:'auto', backgroundColor: "#efefef"}} stikyHeader  aria-label="sticky table">
      <TableHeader displaySelectAll={ false } adjustForCheckbox = {false}>
        <TableRow>
          {header.map((x, i) => (
            <TableHeaderColumn key={`thc-${i}`}>{x.name}</TableHeaderColumn>
          ))}
          <TableHeaderColumn />
          {handleRemove != null ? <TableHeaderColumn /> : null}
        </TableRow>
      </TableHeader>
      <TableBody displayRowCheckbox={ false }>
        {data.map((x, i) =>
          row(
            x,
            i,
            header,
            handleRemove,
            startEditing,
            editIdx,
            handleChange,
            stopEditing, 
            selectable
          )
        )}
      </TableBody>
    </Table>
  </Paper>
);