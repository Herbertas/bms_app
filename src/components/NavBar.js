import React from 'react';
import * as ReactBootStrap from "react-bootstrap";
import {
    // BrowserRouter as Router,
    Link
  } from "react-router-dom";


  

const NavBar = () => {
    return(
        <div className="App">
          <ReactBootStrap.Navbar collapseOnSelect expand="xl" bg="dark" variant="dark">
        <Link to="/home">
        <ReactBootStrap.Navbar.Brand href="#home">Battery Management Systems</ReactBootStrap.Navbar.Brand>
        </Link>
        <ReactBootStrap.Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <ReactBootStrap.Navbar.Collapse id="responsive-navbar-nav">
          <ReactBootStrap.Nav className="mr-auto"> 
          <Link to="/clients">
          <ReactBootStrap.Nav.Link href="#clients">Klientai</ReactBootStrap.Nav.Link>
          </Link>
          <Link to="/products">
          <ReactBootStrap.Nav.Link href="#products">Produktai</ReactBootStrap.Nav.Link>
          </Link>
          <Link to="/storage">
          <ReactBootStrap.Nav.Link href="#storage">Sandėlis</ReactBootStrap.Nav.Link>
          </Link>
          <Link to="/users">
          <ReactBootStrap.Nav.Link href="#users">Sistemos Vartotojai</ReactBootStrap.Nav.Link>
          </Link>
          {/* <Link to="/settings">
          <ReactBootStrap.Nav.Link href="#settings">Nustatymai</ReactBootStrap.Nav.Link>
          </Link> */}
          <Link to="/">
          <ReactBootStrap.Nav.Link href="#login">Logout</ReactBootStrap.Nav.Link>
          </Link>
          </ReactBootStrap.Nav>
        </ReactBootStrap.Navbar.Collapse>
        </ReactBootStrap.Navbar>
        </div>
    )
}
export default NavBar;
