import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
  useHistory,
  Link
} from "react-router-dom";

import * as ReactBootStrap from "react-bootstrap";
import AuthService from "./services/auth.service";
import "./App.css";


import LoginPage    from "./Pages/LoginPage"
import HomePage     from "./Pages/HomePage"
import ProfilePage  from "./Pages/ProfilePage"
import ClientsPage  from "./Pages/ClientsPage"
import ProductsPage from "./Pages/ProductsPage"
import SettingsPage from "./Pages/SettingsPage"
import StoragePage  from "./Pages/StoragePage"
import UsersPage from './Pages/UsersPage';
import PageNotFoundPage from './Pages/PageNotFoundPage'
import { Nav } from 'react-bootstrap';
import AddProductPage from './Pages/AddProductPage'
import AddUserPage from './Pages/AddUserPage'
import AddClientPage from './Pages/AddClientPage'
import AddProductFaultPage from './Pages/AddProductFaultPage'
import AddNewOrderPage from './Pages/AddNewOrderPage'
import OrderPage from './Pages/OrdersPage'
import OffersPage from './Pages/OffersPage'
import AddNewOfferPage from './Pages/AddNewOfferPage'
import AddNewOrderCustomerSelection from './Pages/AddNewOrderCustomerSelectionPage'
class App extends React.Component {

  constructor(props) {
    super(props)
    this.logOut = this.logOut.bind(this);

    this.state = {
      isAdmin: false,
      isSalesman: false,
      isAccountant: false,
      isManufacturer: false,
      isWarehousekeeper: false,
      isCustomerSupportSpecialist: false,
      isDeveloper: false,
      currentUser: null,
    };

  }

  componentDidMount() {
    const user = AuthService.getCurrentUser();
    if (user) {
      this.setState({
        currentUser: user,
        isSalesman: user.userRoles.includes("ROLE_SALESMAN"),
        isManufacturer: user.userRoles.includes("ROLE_MANUFACTURER"), 
        isDeveloper: user.userRoles.includes("ROLE_DEVELOPER"),
        isAdmin: user.userRoles.includes("ROLE_ADMIN"),
        isWarehousekeeper: user.userRoles.includes("ROLE_WAREHOUSEKEEPER"), 
        isCustomerSupportSpecialist: user.userRoles.includes("ROLE_CUSTOMER_SUPPORT_SPECIALIST"),
        isAccountant: user.userRoles.includes("ROLE_ACCOUNTANT")
      });
    }
  }

  logOut() {
    AuthService.logout();
  }

  render() {

    const { currentUser, isSalesman, isManufacturer, isDeveloper, isAdmin, isWarehousekeeper, isCustomerSupportSpecialist, isAccountant} = this.state;
    return (

      <Router>
        <div className="App">
          {(currentUser != null) ?
          <ReactBootStrap.Navbar collapseOnSelect expand="xl" bg="dark" variant="dark">
            {(currentUser != null) && (           
              <Link to="/orders">
                <ReactBootStrap.Navbar.Brand href="#orders"><span className="font-italic">IVPS</span></ReactBootStrap.Navbar.Brand>
              </Link>
            )}
            <ReactBootStrap.Navbar.Toggle aria-controls="responsive-navbar-nav" />
              <ReactBootStrap.Navbar.Collapse id="responsive-navbar-nav">
                <ReactBootStrap.Nav className="mr-auto"> 

                  {(currentUser != null) && (isSalesman || isAdmin || isWarehousekeeper) && (
                    <Link to="/clients">
                    <ReactBootStrap.Nav.Link href="#clients" id="clients">Klientai</ReactBootStrap.Nav.Link>
                    </Link>
                  )}

                  {(currentUser != null) && (isSalesman || isAdmin || isManufacturer || isAccountant || isWarehousekeeper) && (

                    <ReactBootStrap.NavDropdown title="Užsakymai/Pasiūlymai" id="collasible-nav-dropdown Užsakymai">
                      {(isSalesman || isAdmin) ? 
                        <ReactBootStrap.NavDropdown.Item href="/addNewOffer" id="addNewOffer">Pridėti naują pasiūlymą</ReactBootStrap.NavDropdown.Item>
                        :
                        null
                      }
                      {(isSalesman || isAdmin) ?
                        <ReactBootStrap.NavDropdown.Item href="/addOrder" id="addNewOrder">Pridėti naują užsakymą</ReactBootStrap.NavDropdown.Item>
                        : 
                        null
                      }
                      <ReactBootStrap.NavDropdown.Item href="/offers" id="offers">Pasiūlymai</ReactBootStrap.NavDropdown.Item>
                      <ReactBootStrap.NavDropdown.Item href="/orders" id="orders">Užsakymai</ReactBootStrap.NavDropdown.Item>
                    </ReactBootStrap.NavDropdown>
                  )}

                  {(currentUser != null) && (isSalesman || isAdmin || isManufacturer || isDeveloper || isWarehousekeeper) && (
                    <ReactBootStrap.NavDropdown title="Produktai" id="collasible-nav-dropdown">
                      <ReactBootStrap.NavDropdown.Item href="/products" id="getProducts">Produktų informacija </ReactBootStrap.NavDropdown.Item>
                      {(isDeveloper || isAdmin) ? 
                        <ReactBootStrap.NavDropdown.Item href="/addProduct" id="addProduct">Pridėti naują produktą</ReactBootStrap.NavDropdown.Item>
                        : null
                      }
                      <ReactBootStrap.NavDropdown.Item href="/addProductFault" id="addProductFault">Pridėti produkto klaidą</ReactBootStrap.NavDropdown.Item>
                    </ReactBootStrap.NavDropdown>
                  )}

                  {(currentUser != null) && (isSalesman || isAdmin || isManufacturer || isWarehousekeeper) && (
                    <Link to="/storage">
                      <ReactBootStrap.Nav.Link href="#storage" id="storage">Sandėlis</ReactBootStrap.Nav.Link>
                    </Link>
                  )}

                  {(currentUser != null) && (isAdmin) && (
                    <Link to="/users">
                      <ReactBootStrap.Nav.Link id="users" href="#users">Sistemos naudotojai</ReactBootStrap.Nav.Link>
                    </Link>
                  )}

                  {(currentUser != null) ? 
                  (
                    <Link to="/profile">
                      <ReactBootStrap.Nav.Link href="#profile" id="profile">{currentUser.userEmail}</ReactBootStrap.Nav.Link>
                    </Link>
                  )
                  :
                  null}
                  {(currentUser != null) && (
                    <Link to="/login">
                    <ReactBootStrap.Nav.Link  id="logout" href="#login" onClick={this.logOut}>Atsijungti</ReactBootStrap.Nav.Link>
                    </Link>
                  )}
                </ReactBootStrap.Nav>
              </ReactBootStrap.Navbar.Collapse>
          </ReactBootStrap.Navbar>
          : 
          <></>
          }
        </div>
        <Switch>
          <Route exact path={["/login", ""]} component={LoginPage}/>
          <Route exact path="/home" component = {HomePage} />
          <Route path="/offers" component = {OffersPage} />
          <Route path="/orders" component = {OrderPage} />
          <Route path="/profile" component = {ProfilePage} />
          <Route path="/clients" component = {ClientsPage} />
          <Route path="/products" component = {ProductsPage} />
          <Route path="/storage" component = {StoragePage} />
          <Route path="/users" component = {UsersPage} />
          <Route path="/addProduct" component = {AddProductPage} />
          <Route path="/addUser" component = {AddUserPage} />
          <Route path="/addCustomer" component = {AddClientPage} />
          <Route path="/addProductFault" component = {AddProductFaultPage} />
          <Route path="/404" component={PageNotFoundPage} />
          <Route path="/addOrder" component={AddNewOrderCustomerSelection} />
          <Route path="/addNewOffer" component={AddNewOfferPage} />

          <Redirect to='/404' />

        </Switch>
      </Router>
    )

  }

}
export default App;
