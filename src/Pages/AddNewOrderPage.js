import React, { Component } from 'react'
import { Container, Button, Form } from 'react-bootstrap'
import ProductService from '../services/product.service'
import AuthService from '../services/auth.service'
import { Redirect } from "react-router-dom";
import { ThemeConsumer } from 'react-bootstrap/esm/ThemeProvider';
import { form } from 'react-validation/build/form';
import OrderStatusService from '../services/orderStatus.service';
import OrderService from '../services/order.service';
import EmployeeService from '../services/employee.service';
import CurrencyInput from 'react-currency-input-field';
import DatePicker from 'react-datepicker';
import Select from 'react-select'
import companyService from '../services/company.service';


class AddNewOrderPage extends Component {
    constructor(props) {
        super(props)
        console.log(props)
        this.state = {
            redirect: null,
            userReady: false,
            currentUser: { userEmail: "" },
            data: null,
            isLoading: false,
            error: null,

            employeeId: null, 
            // Order VAT Invoice data
            companyId: null,
            salesVATInvoiceVATDate: null, 
            salesVATInvoicePayAmountBeforeVAT: props.offer.offerPrice, 
            salesVATInvoiceAmountToPayBeforeVATInWords: null, 
            salesVATInvoiceVATAmount: null, 
            salesVATInvoicePayAmountAfterVAT: null, 
            salesVATInvoiceAmountToPayAfterVATInWords: null, 
            salesVATInvoiceAmountPayTill: null, 
            
            // Consigment Note data
            consigmentNoteDate: null, 
            consigmentNoteWeight: null, 
            consigmentNoteVolume: null,

            // Order data
            orderStatusId: null,
            orderDate: null,
            orderTillDate: null,
            offerId: null,


            formError : {
                consigmentNoteDate: null,
                consigmentNoteWeight: null,
                consigmentNoteVolume: null,
                employeeId: null, 
                companyId: null, 
                salesVATInvoiceVATDate: null, 
                salesVATInvoicePayAmountBeforeVAT: null, 
                salesVATInvoiceAmountToPayBeforeVATInWords: null, 
                salesVATInvoiceVATAmount: null, 
                salesVATInvoicePayAmountAfterVAT: null, 
                salesVATInvoiceAmountToPayAfterVATInWords: null, 
                salesVATInvoiceAmountPayTill: null, 
                orderStatusId: null, 
                offerId: null, 
                orderDate: null, 
                orderTillDate: null
            },

            formErrorFound : null
        }
         this.handleChangeOrderDate = this.handleChangeOrderDate.bind(this)
         this.handleChangeOrderTillDate = this.handleChangeOrderTillDate.bind(this)
         this.handleChangeOrderStatus = this.handleChangeOrderStatus.bind(this)
         this.handleChangeSalesVATInvoiceAmountToPayBeforeVATInWords = this.handleChangeSalesVATInvoiceAmountToPayBeforeVATInWords.bind(this)
         this.handleChangeSalesVATInvoiceDate = this.handleChangeSalesVATInvoiceDate.bind(this)
         this.handleChangeSalesVATInvoiceAmountPayTill = this.handleChangeSalesVATInvoiceAmountPayTill.bind(this)
         this.handleChangeSalesVATInvoiceAmountToPayAfterVATInWords = this.handleChangeSalesVATInvoiceAmountToPayAfterVATInWords.bind(this)
         this.onSubmit = this.onSubmit.bind(this);


    }

    async getOrderStatuses()
    {
        const res = await OrderStatusService.getAllOrderStatuses()
        const data = res.data;
        const options = data.map(d => ({
            "label" : d.orderStatusName,
            "value" : d._id
      
          }))
  
          this.setState({orderStatusOptions: options})
    }


    async getCurrentUser() {
        const currentUser = await AuthService.getCurrentUser();
        if (!currentUser) {
            this.setState({ redirect: "/login" });
        }
        if (!(currentUser.userRoles.includes("ROLE_ADMIN") || currentUser.userRoles.includes("ROLE_DEVELOPER"))) {
            this.setState({ redirect: "/home" });
        }
        return currentUser;
    }

    async getEmployeeData(currentUser) {

        const res = await EmployeeService.getEmployeeByUserId(currentUser.id)
        console.log(res)

        this.setState({employeeId: res.data._id})
    }

    async getCompany() {
        const res = await companyService.getAllCompanies();
        this.setState({companyId: res.data[0]._id})
    }

    async componentDidMount() {
        console.log(this.props.offerSelectedData)
        this.setState({ data: this.state.usersData, isLoading: true, salesVATInvoicePayAmountBeforeVAT: this.props.offer.offerPrice})
        const currentUser = await this.getCurrentUser();
        await this.getEmployeeData(currentUser);
        await this.getOrderStatuses();
        await this.getCompany();
        
        this.setState({ currentUser: currentUser, userReady: true, isLoading: false})
    }

    async onSubmit(e) {
        e.preventDefault();

        this.findSomeErrors()
        const {consigmentNoteDate, consigmentNoteWeight, consigmentNoteVolume,
            employeeId, companyId, salesVATInvoiceVATDate, salesVATInvoicePayAmountBeforeVAT,
            salesVATInvoiceAmountToPayBeforeVATInWords, salesVATInvoiceVATAmount, salesVATInvoicePayAmountAfterVAT, 
            salesVATInvoiceAmountToPayAfterVATInWords, salesVATInvoiceAmountPayTill, orderStatusId, 
            orderDate, orderTillDate, formErrorFound} = this.state
        
        if(formErrorFound == null)
        {
            OrderService.addNewOrderSalesVATInvoiceConsigmentNote(consigmentNoteDate, consigmentNoteWeight, consigmentNoteVolume,
                this.props.offer.customerId, employeeId, companyId, salesVATInvoiceVATDate, salesVATInvoicePayAmountBeforeVAT,
                salesVATInvoiceAmountToPayBeforeVATInWords, salesVATInvoiceVATAmount, salesVATInvoicePayAmountAfterVAT,
                salesVATInvoiceAmountToPayAfterVATInWords, salesVATInvoiceAmountPayTill, orderStatusId, this.props.offer._id, orderDate, orderTillDate, 
                )
            .then(
                () => {
                    alert("Naujas užsakymas sėkmingai pridėtas!")
                    window.location.reload();
                },
                error => {
                    const resMessage =
                    (error.response &&
                        error.response.data &&
                        error.response.data.message) ||
                    error.message ||
                    error.toString();
        
                    this.setState({
                    loading: false,
                    message: resMessage
                    });
                }
            );
            console.log()

        }
    }

    findSomeErrors = () => {
        const {consigmentNoteDate, consigmentNoteWeight, consigmentNoteVolume,
            salesVATInvoiceVATDate, salesVATInvoicePayAmountBeforeVAT,
            salesVATInvoiceAmountToPayBeforeVATInWords, salesVATInvoiceVATAmount, salesVATInvoicePayAmountAfterVAT, 
            salesVATInvoiceAmountToPayAfterVATInWords, salesVATInvoiceAmountPayTill, orderStatusId, 
            orderDate, orderTillDate, formErrorFound, formError} = this.state


        if (orderDate === null) formError.orderDate = "Šis laukelis privalomas"
        else formError.orderDate = null

        if(orderTillDate === null) formError.orderTillDate = "Šis laukelis privalomas"
        else formError.orderTillDate = null

        if(consigmentNoteWeight === null) formError.consigmentNoteWeight = "Šis laukelis privalomas"
        else formError.consigmentNoteWeight = null

        if(consigmentNoteVolume === null) formError.consigmentNoteVolume = "Šis laukelis privalomas"
        else formError.consigmentNoteVolume = null

        if(salesVATInvoiceVATAmount === null) formError.salesVATInvoiceVATAmount = "Šis laukelis privalomas"
        else formError.salesVATInvoiceVATAmount = null

        if(orderStatusId === null) formError.orderStatusId = "Šis laukelis privalomas"
        else formError.orderStatusId = null

        if(salesVATInvoicePayAmountBeforeVAT === null) formError.salesVATInvoicePayAmountBeforeVAT = "Šis laukelis privalomas"
        else formError.salesVATInvoicePayAmountBeforeVAT = null

        if(salesVATInvoicePayAmountAfterVAT === null) formError.salesVATInvoicePayAmountAfterVAT = "Šis laukelis privalomas"
        else formError.salesVATInvoicePayAmountAfterVAT = null

        if(consigmentNoteDate === null) formError.consigmentNoteDate = "Šis laukelis privalomas"
        else formError.consigmentNoteDate = null

        if(salesVATInvoiceAmountPayTill === null) formError.salesVATInvoiceAmountPayTill = "Šis laukelis privalomas"
        else formError.salesVATInvoiceAmountPayTill = null

        if(salesVATInvoiceVATDate === null) formError.salesVATInvoiceVATDate = "Šis laukelis privalomas"
        else formError.salesVATInvoiceVATDate = null


        if(salesVATInvoiceAmountToPayAfterVATInWords === null) formError.salesVATInvoiceAmountToPayAfterVATInWords = "Šis laukelis privalomas"
        else if(salesVATInvoiceAmountToPayAfterVATInWords.length < 3 || salesVATInvoiceAmountToPayAfterVATInWords.length > 256) formError.salesVATInvoiceAmountToPayAfterVATInWords = "Produkto aprašymo ilgis turi turėti 3 simbolius"
        else formError.salesVATInvoiceAmountToPayAfterVATInWords = null

        if(salesVATInvoiceAmountToPayBeforeVATInWords === null) formError.salesVATInvoiceAmountToPayBeforeVATInWords = "Šis laukelis privalomas"
        else if(salesVATInvoiceAmountToPayBeforeVATInWords.length < 3 || salesVATInvoiceAmountToPayBeforeVATInWords.length > 256) formError.salesVATInvoiceAmountToPayBeforeVATInWords = "Produkto aprašymo ilgis turi turėti 3 simbolius"
        else formError.salesVATInvoiceAmountToPayBeforeVATInWords = null

        if(formError == null)
        {
            this.setState({formErrorFound : true})
        }
        else
        {
            this.setState({formErrorFound : false})
        }
        
    }

    handleChangeOrderDate = (e) => {
        this.setState({
            orderDate : e
        })
    }

    handleChangeOrderTillDate = (e) => {
        this.setState({
            orderTillDate : e
        })
    }


    handleChangeConsigmentNoteDate = (e) => {
        this.setState({
            consigmentNoteDate: e
        })
    }

    handleChangeOrderStatus = (e) => {
        this.setState({
            orderStatusId : e.value
        })
    }

    handleChangeSalesVATInvoiceAmountToPayBeforeVATInWords = (e) => {
        this.setState({
            salesVATInvoiceAmountToPayBeforeVATInWords : e.target.value
        })

    }

    handleChangeSalesVATInvoiceAmountToPayAfterVATInWords = (e) => {
        this.setState({
            salesVATInvoiceAmountToPayAfterVATInWords : e.target.value
        })
    }

    handleChangeSalesVATInvoiceDate = (e) => {
        this.setState({
            salesVATInvoiceVATDate : e
        })
    }

    handleChangeSalesVATInvoiceAmountPayTill = (e) => {
        this.setState({
            salesVATInvoiceAmountPayTill : e
        })
    }


    render() {

        const { currentUser, redirect, error, isLoading } = this.state;


        if (redirect) {
            return <Redirect to={redirect} />
        }

        if (error === true) {
            return <p>{error.message}</p>;
        }

        if (isLoading === true) {
            return <p>Loading...</p>;
        }

        return(
            <Container>
                    <h1 className="text-center">
                        <span className="font-italic" style={{fontSize : '75%'}}>Pridėkite naują užsakymą</span>
                    </h1>
                    <hr/>
                <Form onSubmit={this.onSubmit}>
                    <h4>Užsakymo informacija</h4>

                    <Form.Group>
                        <Form.Label>Užsakymo data* </Form.Label>
                        <br></br>
                        <DatePicker
                            selected={ this.state.orderDate }
                            onChange={ this.handleChangeOrderDate}
                            name="orderDate"
                            dateFormat="MM/dd/yyyy"
                            placeholderText="Pasirinkite datą"
                        />
                        <Form.Control.Feedback type='selected' style={{color: "#dc3545", fontSize: '80%'}}>{this.state.formError.orderDate}</Form.Control.Feedback>
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Užsakymą pateikti iki data* </Form.Label>
                        <br></br>
                        <DatePicker
                            selected={ this.state.orderTillDate }
                            onChange={ this.handleChangeOrderTillDate}
                            name="orderTillDate"
                            dateFormat="MM/dd/yyyy"
                            placeholderText="Pasirinkite datą"

                        />
                        <Form.Control.Feedback type='selected' style={{color: "#dc3545", fontSize: '80%'}}>{this.state.formError.orderTillDate}</Form.Control.Feedback>
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Užsakymo statusas*</Form.Label>
                        <Select options={this.state.orderStatusOptions} onChange={this.handleChangeOrderStatus} placeholder="Pasirinkite užsakymo statusą"/>
                        <Form.Control.Feedback type='selected' style={{color: "#dc3545", fontSize: '80%'}}>{this.state.formError.orderStatusId}</Form.Control.Feedback>
                    </Form.Group>

                    <hr/>
                    <h4>Važtaraščio informacija</h4>


                    <Form.Group>
                        <Form.Label>Važtaraščio data* </Form.Label>
                        <br></br>
                        <DatePicker
                            selected={ this.state.consigmentNoteDate}
                            onChange={ this.handleChangeConsigmentNoteDate}
                            name="offerValidFromDate"
                            dateFormat="MM/dd/yyyy"
                            placeholderText="Pasirinkite datą"

                        />
                        <Form.Control.Feedback type='selected' style={{color: "#dc3545", fontSize: '80%'}}>{this.state.formError.consigmentNoteDate}</Form.Control.Feedback>
                    </Form.Group>

                    
                    <Form.Group>
                        <Form.Label>Svoris*</Form.Label>
                        <br></br>
                        <CurrencyInput
                            placeholder="Įveskite svorį"
                            decimalsLimit={1}
                            id="Svoris"
                            suffix={"g"}
                            onValueChange={(value, name) => {
                                this.setState({
                                    consigmentNoteWeight: value
                                })
                                console.log(value)
                            }}>     
                        </CurrencyInput>
                        <Form.Control.Feedback type='selected' style={{color: "#dc3545", fontSize: '80%'}}>{this.state.formError.consigmentNoteWeight}</Form.Control.Feedback>
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Tūris*</Form.Label>
                        <br></br>
                        <CurrencyInput
                            placeholder="Įveskite tūri"
                            id="Tūris"
                            decimalsLimit={1}
                            suffix={"cm³"}
                            disableAbbreviations={true}
                            onValueChange={(value, name) => {
                                this.setState({
                                    consigmentNoteVolume: value
                                })
                                console.log(value)
                            }}>     
                        </CurrencyInput>
                        <Form.Control.Feedback type='selected' style={{color: "#dc3545", fontSize: '80%'}}>{this.state.formError.consigmentNoteVolume}</Form.Control.Feedback>
                    </Form.Group>

                    <br></br>
                    <hr/>
                    <h4>PVM sąskaitos faktūros informacija</h4>
                    <Form.Group>
                        <Form.Label>Suma iki mokesčių*</Form.Label>
                        <br></br>
                        <CurrencyInput
                            placeholder="Įveskitę produkto kainą"
                            decimalsLimit={2}
                            suffix={"€"}
                            defaultValue={this.props.offer.offerPrice}
                            onValueChange={(value, name) => {
                                if(this.state.salesVATInvoiceVATAmount)
                                {
                                    var afterVAT = value + (value * (this.state.salesVATInvoiceVATAmount / 100))
                                    this.setState({
                                        salesVATInvoicePayAmountBeforeVAT: value,
                                        salesVATInvoicePayAmountAfterVAT: afterVAT
                                    })
                                }
                                else
                                {
                                    this.setState({
                                        salesVATInvoicePayAmountBeforeVAT: value
                                    })
                                }

                            }}>     
                        </CurrencyInput>
                        <Form.Control.Feedback type='selected' style={{color: "#dc3545", fontSize: '80%'}}>{this.state.formError.salesVATInvoicePayAmountBeforeVAT}</Form.Control.Feedback>
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Suma iki mokesčių žodžiais*</Form.Label>
                        <Form.Control placeholder="Įveskite užsakymo kainą eurais" onChange={this.handleChangeSalesVATInvoiceAmountToPayBeforeVATInWords} isInvalid={this.state.formError.salesVATInvoiceAmountToPayBeforeVATInWords}/>
                        <Form.Control.Feedback type='invalid'>{this.state.formError.salesVATInvoicePayAmountBeforeVAT}</Form.Control.Feedback>
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Mokestis procentais*</Form.Label>
                        <br></br>
                        <CurrencyInput
                            placeholder="Įveskitę PVM mokesčio dydi"
                            decimalsLimit={2}
                            suffix={"%"}
                            onValueChange={(value, name) => {
                                var afterVAT = this.state.salesVATInvoicePayAmountBeforeVAT + (this.state.salesVATInvoicePayAmountBeforeVAT * (value / 100))
                                console.log(afterVAT)
                                this.setState({salesVATInvoiceVATAmount: value, salesVATInvoicePayAmountAfterVAT: afterVAT});
                            }}>     
                        </CurrencyInput>
                        <br></br>
                        {this.state.salesVATInvoiceVATAmount &&  this.state.salesVATInvoicePayAmountBeforeVAT?
                            <Form.Control.Feedback type='selected'>Suma po mokesčių: {this.state.salesVATInvoicePayAmountAfterVAT} €</Form.Control.Feedback>
                            : 
                            <Form.Control.Feedback type='selected' style={{color: "#dc3545", fontSize: '80%'}}>{this.state.formError.salesVATInvoiceVATAmount}</Form.Control.Feedback>
                        }
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Suma po mokesčių žodžiais*</Form.Label>
                        <Form.Control placeholder="Patvirtinkite įvesdami sumą po mokesčių žodžiais" onChange={this.handleChangeSalesVATInvoiceAmountToPayAfterVATInWords} isInvalid={this.state.formError.salesVATInvoiceAmountToPayAfterVATInWords}/>
                        <Form.Control.Feedback type='invalid'>{this.state.formError.salesVATInvoiceAmountToPayAfterVATInWords}</Form.Control.Feedback>
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Sąskaitos faktūros išrašymo data: *</Form.Label>
                        <br></br>
                        <DatePicker
                            selected={ this.state.salesVATInvoiceVATDate }
                            onChange={ this.handleChangeSalesVATInvoiceDate}
                            name="salesVATInvoiceVATDate"
                            dateFormat="MM/dd/yyyy"
                            placeholderText="Pasirinkite datą"

                        />
                        <Form.Control.Feedback type='selected' style={{color: "#dc3545", fontSize: '80%'}}>{this.state.formError.salesVATInvoiceVATDate}</Form.Control.Feedback>
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Sumokėti iki data: *</Form.Label>
                        <br/>
                        <DatePicker
                            selected={ this.state.salesVATInvoiceAmountPayTill }
                            onChange={ this.handleChangeSalesVATInvoiceAmountPayTill}
                            name="orderInvoicePayTillDate"
                            dateFormat="MM/dd/yyyy"
                            placeholderText="Pasirinkite datą"

                        />
                        <Form.Control.Feedback type='selected' style={{color: "#dc3545", fontSize: '80%'}}>{this.state.formError.salesVATInvoiceAmountPayTill}</Form.Control.Feedback>
                    </Form.Group>

                    <Button variant="secondary" type="submit">
                        Pridėti
                    </Button>
                </Form>
                     
            </Container>
        )
    }
}
export default AddNewOrderPage;