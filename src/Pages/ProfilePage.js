import { MuiThemeProvider } from 'material-ui/styles'
import React, { Component } from 'react'
import { Button, Container } from 'react-bootstrap'
import AuthService from "../services/auth.service";
import { Redirect } from "react-router-dom";


class ProfilePage extends Component {
    constructor(props) {
        super(props)

    this.state = {
        redirect: null,
        userReady: false,
        currentUser: { userEmail: "" }
    };
    }

    componentDidMount() {
        const currentUser = AuthService.getCurrentUser();
    
        if (!currentUser)
        {
            this.setState({ redirect: "/login" });
        } 
        this.setState({ currentUser: currentUser, userReady: true })
    }

    render() {
        if (this.state.redirect) {
            return <Redirect to={this.state.redirect} />
        }
      
        const { currentUser } = this.state;
        return(
                <Container>
                    <h1 className="text-center">
                        <span className="font-italic" style={{fontSize : '75%'}}>Profilis</span>
                    </h1>
                    <hr/>

                    {(this.state.userReady) ?
                        (
                        <div>
                        <p>
                            <strong>Token:</strong>{" "}
                            {currentUser.userAccessToken.substring(0, 20)} ...{" "}
                            {currentUser.userAccessToken.substr(currentUser.userAccessToken.length - 20)}
                        </p>
                        <p>
                            <strong>Id:</strong>{" "}
                            {currentUser.id}
                        </p>
                        <p>
                            <strong>Email:</strong>{" "}
                            {currentUser.userEmail}
                        </p>
                        <strong>Authorities:</strong>
                        <ul>
                            {currentUser.userRoles &&
                            currentUser.userRoles.map((userRole, index) => <li key={index}>{userRole}</li>)}
                        </ul>
                        </div>
                        )
                        :
                        null}
                    
                </Container>
        )
    }
}
export default ProfilePage;