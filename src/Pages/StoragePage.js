import React, { Component } from 'react'
import { Container, Row, Col, Form, Button} from 'react-bootstrap'
import Table from '../components/Table'
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import StorageService from '../services/storage.service'
import AuthService from '../services/auth.service'
import { Redirect } from "react-router-dom";
import Select from 'react-select'


class StoragePage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            redirect: null,
            userReady: false,
            currentUser: { userEmail: "" },
            data: null,
            editIdx : -1,
            isLoading: true,
            error: null,

            storagesSelectionOptions: [], 
            selectedStorageId: null,
            selectedStorageAdresss: null
        };

        this.handleChangeStorage = this.handleChangeStorage.bind(this)

    }

    fetchStorageData = async() => {
        for(var i = 0; i < 5; i++)
        {
            await StorageService.getAllProductsFromParticularStorage(this.state.selectedStorageId).then(
                response => {
                    if(response.data.length > 0)
                    {
                        for(var i = 0; i < response.data.length; i++)
                        {
                            response.data[i].productCode = response.data[i].product.productCode;
                            response.data[i].productName = response.data[i].product.productName;
                        }
                        this.setState({data: response.data})
                        i = 5;
                    }

                },
                error => {
                    this.setState({
                        error,
                    })
                }
            );

        }

    }


    componentDidMount = async() => {

        this.setState({isLoading: true});

        const currentUser =  await AuthService.getCurrentUser();

        if (!currentUser)
        {
            this.setState({ redirect: "/login" });
            return
        }

        // getting all storages

        await StorageService.getAllStorages().then(
            response => {

                const storagesSelectionOptions = response.data.map(d => ({
                    "label" : d.storageAdress,
                    "value" : d._id
              
                  }))
                this.setState({storagesSelectionOptions: storagesSelectionOptions, selectedStorageId: response.data[0]._id})
            }, 
            error => {
                this.setState({
                    error,
                    isLoading: false
                })
                console.log(error)
            }
        );         
        // getting all products from particular storage

        await this.fetchStorageData();

        this.setState({ currentUser: currentUser, userReady: true, isLoading: false})
    }
    
    startEditing = i => {
    this.setState({ editIdx: i });
    };

    stopEditing = () => {
    this.setState({ editIdx: -1 });
    };

    handleChange = (e, name, i) => {
        const { value } = e.target;
        this.setState(state => ({
            data: state.data.map(
            (row, j) => (j === i ? { ...row, [name]: value } : row)
            )
        }));
    };

    async handleChangeStorage(e){
        this.setState({
            selectedStorageAdresss : e.label,
            selectedStorageId : e.value
        })

        console.log(this.state.selectedStorageId)

        await this.fetchStorageData();
    }

    handleAdd = () => {
        window.location = '/addProduct'
    }

    render() {

        const { currentUser, redirect, error, isLoading } = this.state;

        if (redirect) {
            return <Redirect to={redirect} />
        }

        if (error === true) {
            return <p>{error.message}</p>;
          }

        if (isLoading === true) {
            return <p>Loading...</p>;
        }
        
        return(
            <Container fluid>
                    <h1 className="text-center">
                        <span className="font-italic" style={{fontSize : '75%'}}>Sandėlis</span>
                    </h1>
                    <hr/>

                    <Row>
                        <Col xs={9} md={5} lg={4}><Form.Label>Sandėlio adresas</Form.Label></Col>
                    </Row>
                    <Row>
                        <Col xs={12} md={7} lg={6}>
                        <Select options={this.state.storagesSelectionOptions} onChange={this.handleChangeStorage} defaultValue={this.state.storagesSelectionOptions[0]}/>
                        </Col>
                        <Col xs={12} md={5} lg={3}>
                                <Button className="btn-m btn-secondary btn-block" size="md" type="submit" onClick={() => this.handleAdd()}>Pridėti naują produktą į sandelį</Button>
                        </Col>
                    </Row>
                    <br></br>
                    <Row>
                        <Col>
                        <MuiThemeProvider>
                            <Table
                                startEditing={this.startEditing}
                                editIdx={this.state.editIdx}
                                stopEditing={this.stopEditing}
                                handleChange={this.handleChange}
                                data={this.state.data}
                                header={[
                                {
                                    name: "Pavadinimas",
                                    prop: "productName"
                                },
                                {
                                    name: "Produkto numeris",
                                    prop: "productCode"
                                },
                                {
                                    name: "Likutis sandėlyje",
                                    prop: "storedUnitProductQuantity"
                                },
                                {
                                    name: "Užsakytas kiekis",
                                    prop: "storedUnitProductOrderedQuantity"
                                }, 
                                {
                                    name: "Gaminamas kiekis",
                                    prop: "storedUnitProductManufacturingQuantity"
                                }, 
                                {
                                    name: "Defektus turintis kiekis", 
                                    prop: "storedUnitProductDefectiveQuantity"
                                }
                                ]}
                            />
                        </MuiThemeProvider>
                        </Col>
                    </Row>
            </Container>
        )
    }
}
export default StoragePage;