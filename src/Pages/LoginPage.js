import React, { Component } from "react";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import AuthService from "../services/auth.service";

const required = value => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        Šis laukelis negali būti tuščias
      </div>
    );
  }
};

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.handleLogin = this.handleLogin.bind(this);
    this.onChangeUserEmail = this.onChangeUserEmail.bind(this);
    this.onChangeUserPassword = this.onChangeUserPassword.bind(this);

    this.state = {
      userEmail: "",
      userPassword: "",
      loading: false,
      message: ""
    };
  }

  onChangeUserEmail(e) {
    this.setState({
      userEmail: e.target.value
    });
  }

  onChangeUserPassword(e) {
    this.setState({
      userPassword: e.target.value
    });
  }

  handleLogin(e) {
    e.preventDefault();

    this.setState({
      message: "",
      loading: true
    });

    this.form.validateAll();

    if (this.checkBtn.context._errors.length === 0) {
      AuthService.login(this.state.userEmail, this.state.userPassword).then(
        () => {
          this.props.history.push("/home");
          window.location.reload();
        },
        error => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();

          this.setState({
            loading: false,
            message: resMessage
          });
        }
      );
    } else {
      this.setState({
        loading: false
      });
    }
  }

  render() {
    return (
      
      <div className="col-md-12">
        <div className="card card-container" style={{ backgroundColor: "#efefef"}}>
          <h1 className="text-center">
            <span className="font-italic">IVPS</span>
          </h1>
          <Form
            onSubmit={this.handleLogin}
            ref={c => {
              this.form = c;
            }}
          >
            <div className="form-group">
              <label htmlFor="userEmail">Elektroninis paštas</label>
              <Input
                type="text"
                className="form-control"
                name="username"
                value={this.state.userEmail}
                onChange={this.onChangeUserEmail}
                validations={[required]}
                placeholder="Įveskite elektroninį paštą"
              />
            </div>

            <div className="form-group">
              <label htmlFor="userPassword">Slaptažodis</label>
              <Input
                type="password"
                className="form-control"
                name="password"
                value={this.state.userPassword}
                onChange={this.onChangeUserPassword}
                validations={[required]}
                placeholder="Įveskite slaptažodį"
              />
            </div>

            <div className="form-group">
              <button
                className="btn-m btn-secondary btn-block"
                disabled={this.state.loading}
              >
                {this.state.loading && (
                  <span className="spinner-border spinner-border-sm"></span>
                )}
                <span>Prisijungti</span>
              </button>
            </div>

            {this.state.message && (
              <div className="form-group">
                <div className="alert alert-danger" role="alert">
                  {this.state.message}
                </div>
              </div>
            )}
            <CheckButton
              style={{ display: "none" }}
              ref={c => {
                this.checkBtn = c;
              }}
            />
          </Form>
        </div>
      </div>

    );
  }
}