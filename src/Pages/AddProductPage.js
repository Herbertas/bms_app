import React, { Component } from 'react'
import { Container, Button, Form } from 'react-bootstrap'
import ProductService from '../services/product.service'
import AuthService from '../services/auth.service'
import { Redirect } from "react-router-dom";
import { ThemeConsumer } from 'react-bootstrap/esm/ThemeProvider';
import { form } from 'react-validation/build/form';
import CurrencyInput from 'react-currency-input-field';


class AddProductPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            redirect: null,
            userReady: false,
            currentUser: { userEmail: "" },
            data: null,
            isLoading: false,
            error: null,

            productName: null, 
            productCode: null, 
            productDescription: null, 
            productWeight: null, 
            productStandardPrice: null,

            formError : {
                productName : null, 
                productCode : null, 
                productDescription: null, 
                productWeight: null, 
                productStandardPrice: null
            },

            formErrorFound : null
        }
         this.handleChangeProductName = this.handleChangeProductName.bind(this)
         this.handleChangeProductCode = this.handleChangeProductCode.bind(this)
         this.handleChangeProductDescription = this.handleChangeProductDescription.bind(this)
         this.handleChangeProductWeight = this.handleChangeProductWeight.bind(this)
         this.onSubmit = this.onSubmit.bind(this);


    }

    findSomeErrors = () => {
        const {formError, productName, productCode, productDescription, productStandardPrice, productWeight} = this.state;

        if (productName === null) formError.productName = "Šis laukelis privalomas"
        else if(productName.length < 3) formError.productName = "Produkto pavadinimas turi būti ilgesnis nei 3 simboliai"
        else formError.productName = null

        if(productCode === null) formError.productCode = "Šis laukelis privalomas"
        else if(productCode.length < 3) formError.productCode = "Produkto kodas turi būti ilgesnis nei 3 simboliai"
        else formError.productCode = null

        if(productDescription === null) formError.productDescription = "Šis laukelis privalomas"
        else if(productDescription.length < 3 || productDescription.length > 256) formError.productDescription = "Produkto aprašymo ilgis turi būti nuo 3 iki 256 simbolių"
        else formError.productDescription = null

        if(productStandardPrice === null) formError.productStandardPrice = "Šis laukelis privalomas"
        else if(isNaN(productStandardPrice)) formError.productStandardPrice = "Turi būti pateiktas skaičius"
        else formError.productStandardPrice = null

        if(productWeight === null) formError.productWeight = "Šis laukelis privalomas"
        else if(isNaN(productWeight)) formError.productWeight = "Turi būti pateiktas skaičius"
        else formError.productWeight = null

        if(formError.productCode || formError.productName || formError.productStandardPrice || formError.productWeight || formError.productDescription)
        {
            this.setState({formErrorFound : true})
        }
        else
        {
            this.setState({formErrorFound : false})
        }
        
    }

    handleChangeProductName = (e) => {
        this.setState({
            productName : e.target.value
        })
    }


    handleChangeProductCode = (e) => {
        this.setState({
            productCode : e.target.value
        })
    }

    handleChangeProductDescription = (e) => {
        this.setState({
            productDescription : e.target.value
        })
    }

    handleChangeProductWeight = (e) => {
        this.setState({
            productWeight : e.target.value
        })
    }

    async onSubmit(e) {
        e.preventDefault();

        this.findSomeErrors();
        
        if(!this.state.formErrorFound)
        {
            const product = {
                productCode: this.state.productCode, 
                productName: this.state.productName, 
                productDescription: this.state.productDescription, 
                productWeight: this.state.productWeight, 
                productStandardPrice: this.state.productStandardPrice
            }
    
            ProductService.addNewProduct(product.productCode, product.productName, product.productDescription,
                product.productWeight, product.productStandardPrice).then(
                    () => {
                        alert("Naujas produktas sėkmingai pridėtas!")
                        window.location.reload();
                    },
                    error => {
                      const resMessage =
                        (error.response &&
                          error.response.data &&
                          error.response.data.message) ||
                        error.message ||
                        error.toString();
            
                      this.setState({
                        loading: false,
                        message: resMessage
                      });
                    }
                );
        }

    
    }


    async componentDidMount() {
        this.setState({ data: this.state.usersData, isLoading: true})
        const currentUser =  await AuthService.getCurrentUser();

        if (!currentUser)
        {
            this.setState({ redirect: "/login" });
        }
        if(!(currentUser.userRoles.includes("ROLE_ADMIN") || currentUser.userRoles.includes("ROLE_DEVELOPER")))
        {
            this.setState({redirect: "/home" })
        }
        this.setState({ currentUser: currentUser, userReady: true, isLoading: false})
    }

    render() {

        const { currentUser, redirect, error, isLoading } = this.state;


        if (redirect) {
            return <Redirect to={redirect} />
        }

        if (error === true) {
            return <p>{error.message}</p>;
          }

        if (isLoading === true) {
            return <p>Loading...</p>;
        }
        return(
            <Container>
                    <h1 className="text-center">
                        <span className="font-italic" style={{fontSize : '75%'}}>Pridėkite naują produktą</span>
                    </h1>
                    <hr/>

                    <Form onSubmit={this.onSubmit}>
                
                        <Form.Group>
                            <Form.Label>Produkto pavadinimas*</Form.Label>
                            <Form.Control placeholder="Įveskite produkto pavadinimą" onChange={this.handleChangeProductName} isInvalid={this.state.formError.productName}/>
                            {(this.state.formError.productName) ?
                                <Form.Control.Feedback type='invalid'>{this.state.formError.productName}</Form.Control.Feedback>
                                : 
                                <Form.Text>Produkto pavadinimas turi būti unikalus ir ilgesnis nei 3 simboliai</Form.Text>
                            }
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Produkto kodas*</Form.Label>
                            <Form.Control placeholder="Įveskite produkto kodą" onChange={this.handleChangeProductCode} isInvalid={this.state.formError.productCode}/>
                            {(this.state.formError.productCode) ? 
                                <Form.Control.Feedback type='invalid'>{this.state.formError.productCode}</Form.Control.Feedback>
                                :
                                <Form.Text>Produkto kodas turi būti unikalus ir ilgesnis nei 3 simboliai</Form.Text>
                            }
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Produkto svoris* (g.)</Form.Label>
                            <Form.Control placeholder="Įveskite produkto svorį gramais" onChange={this.handleChangeProductWeight} isInvalid={this.state.formError.productWeight}/>
                            <Form.Control.Feedback type='invalid'>{this.state.formError.productWeight}</Form.Control.Feedback>
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Produkto aprašymas*</Form.Label>
                            <Form.Control  as="textarea" rows={3} placeholder="Įveskite produkto aprašymą" onChange={this.handleChangeProductDescription} isInvalid={this.state.formError.productDescription}/>
                            <Form.Control.Feedback type='invalid'>{this.state.formError.productDescription}</Form.Control.Feedback>
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Produkto standartinė kaina* (Eur.)</Form.Label>
                            <br></br>
                            <CurrencyInput
                              placeholder="Įveskitę produkto kainą"
                              decimalsLimit={2}
                              suffix={"€"}
                              onValueChange={(value) => {
                                  this.setState({
                                    productStandardPrice : value
                                  })
                              }}>     
                            </CurrencyInput>
                            <Form.Control.Feedback type='selected' style={{color: "#dc3545", fontSize: '80%'}}>{this.state.formError.productStandardPrice}</Form.Control.Feedback>

                        </Form.Group>

                        <Button variant="secondary" type="submit">
                            Pridėti
                        </Button>
                    </Form>
                     
            </Container>
        )
    }
}
export default AddProductPage;