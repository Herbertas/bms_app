import React, { Component } from 'react'
import { Container, Button, Form } from 'react-bootstrap'
import ProductService from '../services/product.service'
import ProductFaultService from '../services/productFault.service'
import ProductFaultStatusService from '../services/productFaultStatus.service'
import EmployeeService from '../services/employee.service'
import AuthService from '../services/auth.service'
import CustomerService from '../services/customers.service'
import { Redirect } from "react-router-dom";
import { ThemeConsumer } from 'react-bootstrap/esm/ThemeProvider';
import Select from 'react-select'
import { form } from 'react-validation/build/form'



class AddProductFaultPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            redirect: null,
            userReady: false,
            currentUser: { userEmail: "" },
            data: null,
            isLoading: false,
            error: null,
            
            productsSelectionOptions : [],
            productFaultProductName: null, 
            productFaultProductId: null, 

            productsFaultStatusSelectionOptions: [],
            productFaultProductFaultStatusName: null, 
            productFaultProductFaultStatusId: null,

            customerSelectionOptions : [],
            productFaultCustomerName: null, 
            productFaultCustomerId: null, 

            productFaultDescription : null,

            productFaultEmployeeId : null,

            formError : {
                productFaultProductId : null, 
                productFaultProductFaultStatusId : null, 
                productFaultCustomerId: null, 
                productFaultDescription: null, 
            },

            formErrorFound : null

        }

         this.handleChangeCustomer = this.handleChangeCustomer.bind(this)
         this.handleChangeProducts = this.handleChangeProducts.bind(this)
         this.handleChangeProductFaultStatus = this.handleChangeProductFaultStatus.bind(this)
         this.handleChangeProductFaultDescription = this.handleChangeProductFaultDescription.bind(this)
         this.onSubmit = this.onSubmit.bind(this);


    }

    findSomeErrors = () => {

        const {formError, productFaultDescription, productFaultProductId,
            productFaultProductFaultStatusId, productFaultCustomerId} = this.state

        if(productFaultProductId ===  null)
        {
            formError.productFaultProductId = "Šis laukelis privalomas"
        }
        else
        {
            formError.productFaultProductId = null
        }
        if(productFaultProductFaultStatusId === null)
        {
            formError.productFaultProductFaultStatusId = "Šis laukelis privalomas"
        }
        else
        {
            formError.productFaultProductFaultStatusId = null
        }

        if(productFaultCustomerId === null)
        {
            formError.productFaultCustomerId = "Šis laukelis privalomas"
        }
        else
        {
            formError.productFaultCustomerId = null
        }

        if(productFaultDescription === null)
        {
            formError.productFaultDescription = "Šis laukelis privalomas"
        }
        else if(productFaultDescription.length < 3 || productFaultDescription.length > 256)
        {
            formError.productFaultDescription = "Produkto klaidos aprašymo ilgis turi būti nuo 3 iki 256 simbolių"
        }
        else 
        {
            formError.productFaultDescription = null
        }
        if(formError.productFaultProductId || formError.productFaultProductFaultStatusId || formError.productFaultCustomerId ||
            formError.productFaultDescription)
        {
            this.setState({formErrorFound : true})
        }
        else
        {
            this.setState({formErrorFound: false})
        }

    }

    async getEmployeeData() {
        const res = await EmployeeService.getEmployeeByUserId(this.state.currentUser.id)
        console.log(res)
        this.setState({productFaultEmployeeId: res.data._id})
    }
    async getAllCustomers(){
        const res = await CustomerService.getAllCustomers();
        const data = res.data
        const options = data.map(d => ({
          "label" : d.customerCompanyName,
          "value" : d._id
    
        }))

        this.setState({customerSelectionOptions: options})
    }

    async  getAllProductFaultStatuses() {
        const res = await ProductFaultStatusService.getAllProductFaultStatuses();
        const data = res.data
        const options = data.map(d => ({
          "label" : d.productFaultStatusName,
          "value" : d._id
    
        }))

        this.setState({productsFaultStatusSelectionOptions: options})
    }

    async getAllProducts(){
        const res = await ProductService.getAllProducts();
        const data = res.data
        const options = data.map(d => ({
          "label" : d.productName,
          "value" : d._id
    
        }))

        this.setState({productsSelectionOptions: options, isLoading: false})
    }

    handleChangeCustomer = (e) => {
        console.log(e)
        this.setState({
            productFaultCustomerName : e.label,
            productFaultCustomerId: e.value
        })
    }

    handleChangeProducts = (e) => {
        console.log(e)
        this.setState({
            productFaultProductName : e.label,
            productFaultProductId: e.value
        })
    }

    handleChangeProductFaultStatus = (e) => {
        console.log(e)
        this.setState({
            productFaultProductFaultStatusName : e.label,
            productFaultProductFaultStatusId: e.value
        })
    }
    handleChangeProductFaultDescription = (e) => {
        this.setState({
            productFaultDescription : e.target.value
        })
    }

    async onSubmit(e) {
        e.preventDefault();

        this.findSomeErrors();
        if(this.state.formErrorFound === null)
        {
            const productFault = {
                productFaultProductFaultStatusId: this.state.productFaultProductFaultStatusId, 
                productFaultCustomerId: this.state.productFaultCustomerId, 
                productFaultEmployeeId: this.state.productFaultEmployeeId, 
                productFaultProductId: this.state.productFaultProductId, 
                productFaultDescription: this.state.productFaultDescription
            }
    
            ProductFaultService.addProductFault(productFault.productFaultProductId, productFault.productFaultProductFaultStatusId,
                productFault.productFaultCustomerId, productFault.productFaultDescription, productFault.productFaultEmployeeId).then(
                    () => {
                        alert("Nauja produkto klaida sėkmingai pridėta!")
                        window.location.reload();
                    },
                    error => {
                      const resMessage =
                        (error.response &&
                          error.response.data &&
                          error.response.data.message) ||
                        error.message ||
                        error.toString();
            
                      this.setState({
                        loading: false,
                        message: resMessage
                      });
                    }
            );

        }
    }


    async componentDidMount() {
        this.setState({ data: this.state.usersData, isLoading: true})
        const currentUser =  await AuthService.getCurrentUser();

        if (!currentUser)
        {
            this.setState({ redirect: "/login" });
        }
        this.setState({ currentUser: currentUser, userReady: true, productFaultEmployeeId: currentUser.employeeId})

        await this.getAllCustomers();
        await this.getAllProductFaultStatuses();
        await this.getEmployeeData();
        await this.getAllProducts();
    }

    render() {

        const { currentUser, redirect, error, isLoading } = this.state;


        if (redirect) {
            return <Redirect to={redirect} />
        }

        if (error === true) {
            return <p>{error.message}</p>;
          }

        if (isLoading === true) {
            return <p>Loading...</p>;
        }
        return(
            <Container>
                    <h1 className="text-center">
                        <span className="font-italic" style={{fontSize : '75%'}}>Pridėkite naują produkto klaidą</span>
                    </h1>
                    <hr/>

                    <Form onSubmit={this.onSubmit}>
                
                        <Form.Group>
                            <Form.Label>Klientas*</Form.Label>
                            <Select options={this.state.customerSelectionOptions} onChange={this.handleChangeCustomer} placeholder="Pasirinkite klientą"/>
                            <Form.Control.Feedback type='selected' style={{color: "#dc3545", fontSize: '80%'}}>{this.state.formError.productFaultCustomerId}</Form.Control.Feedback>
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Produktas*</Form.Label>
                            <Select options={this.state.productsSelectionOptions} onChange={this.handleChangeProducts} placeholder="Pasirinkite produktą"/>
                            <Form.Control.Feedback type='selected' style={{color: "#dc3545", fontSize: '80%'}}>{this.state.formError.productFaultProductId}</Form.Control.Feedback>

                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Produkto klaidos statusas*</Form.Label>
                            <Select options={this.state.productsFaultStatusSelectionOptions} onChange={this.handleChangeProductFaultStatus} placeholder="Pasirinkite produkto klaidos statusą"/>
                            <Form.Control.Feedback type='selected' style={{color: "#dc3545", fontSize: '80%'}}>{this.state.formError.productFaultProductFaultStatusId}</Form.Control.Feedback>
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Produkto klaidos aprašymas*</Form.Label>
                            <Form.Control  as="textarea" rows={3} placeholder="Įveskite produkto klaidos aprašymą" onChange={this.handleChangeProductFaultDescription} isInvalid={this.state.formError.productFaultDescription}/>
                            <Form.Control.Feedback type='invalid'>{this.state.formError.productFaultDescription}</Form.Control.Feedback>
                        </Form.Group>

                        <Button variant="secondary" type="submit">
                            Pridėti
                        </Button>
                    </Form>
                     
            </Container>
        )
    }
}
export default AddProductFaultPage;