import React, { Component } from 'react'
import { Container, Row, Col} from 'react-bootstrap'
import NavBar from '../components/NavBar'
import Table from '../components/Table'
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
class SettingsPage extends Component {
    constructor(props) {
        super(props)
    }


    state = {
        data: [{firstName: "Herbertas", lastName: "Herbertas", password: "Herbertas", position: "Software Engineer", email: "email" }, 
            {firstName: "Jonas", lastName: "Jonas", password: "Jonas", position: "Software Engineer", email: "Jonas" }   ],
        editIdx : -1
    };

    handleRemove = i => {
        this.setState(state => ({
          data: state.data.filter((row, j) => j !== i)
        }));
      };
    
    startEditing = i => {
    this.setState({ editIdx: i });
    };

    stopEditing = () => {
    this.setState({ editIdx: -1 });
    };

    handleChange = (e, name, i) => {
        const { value } = e.target;
        this.setState(state => ({
            data: state.data.map(
            (row, j) => (j === i ? { ...row, [name]: value } : row)
            )
        }));
    };

    render() {
        return(
            <Container>
                <Row>
                    <Col>
                        <h1 className="text-center">
                            <span className="font-italic">Sistemos vartotojai</span>
                        </h1>
                        <MuiThemeProvider>
                            <Table
                                handleRemove={this.handleRemove}
                                startEditing={this.startEditing}
                                editIdx={this.state.editIdx}
                                stopEditing={this.stopEditing}
                                handleChange={this.handleChange}
                                data={this.state.data}
                                header={[
                                {
                                    name: "Vardas",
                                    prop: "userFirstName"
                                },
                                {
                                    name: "Pavardė",
                                    prop: "userLastName"
                                },
                                {
                                    name: "Vartotojo pareigos",
                                    prop: "userRoles"
                                },
                                {
                                    name: "El. paštas",
                                    prop: "email"
                                }, 
                                {
                                    name: "Samdymo data", 
                                    prop: "employeeHiredDate"
                                },
                                {
                                    name: "Atleidimo data", 
                                    prop: "employeeFiredDate"
                                },
                                {
                                    name: "Gyvenamosios vietos adresas",
                                    prop: "employeeAdress"
                                }, 
                                {
                                    name: "Telefono numeris", 
                                    prop: "employeePhoneNumber"
                                }
                                
                                ]}
                            />
                         </MuiThemeProvider>
                    </Col>
                </Row>
            </Container>
        )
    }
}
export default SettingsPage;