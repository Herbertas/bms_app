import React, { Component } from 'react'
import { Container, Row, Col, Button } from 'react-bootstrap'
import NavBar from '../components/NavBar'
import Table from '../components/Table'
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import ProductService from '../services/product.service';
import AuthService from '../services/auth.service'; 
import { Redirect } from "react-router-dom";
import orderService from '../services/order.service';

class OrdersPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            redirect: null,
            userReady: false,
            currentUser: { userEmail: "" },
            editIdx : -1,
            isLoading: true,
            error: null, 

            data: null
        };
    }

    async componentDidMount(){

        this.setState({isLoading: true});
        const currentUser =  await AuthService.getCurrentUser();

        if (!currentUser)
        {
            this.setState({ redirect: "/login" });
        }

        for(var a = 0; a < 5; a++)
        {

            var response = await orderService.getAllOrders(); 
            if(response.data)
            {
                for(var i = 0; i < response.data.length; i++)
                {
                    response.data[i].customerCompanyName = response.data[i].customer.customerCompanyName
                    response.data[i].salesVATInvoiceVATDate = new Date(response.data[i].salesVATInvoice.salesVATInvoiceVATDate).toLocaleDateString();
                    response.data[i].salesVATInvoiceAmountPayTill = new Date(response.data[i].salesVATInvoice.salesVATInvoiceAmountPayTill).toLocaleDateString();
                    response.data[i].salesVATInvoicePayAmountAfterVAT = response.data[i].salesVATInvoice.salesVATInvoicePayAmountAfterVAT
                    response.data[i].offerQuantity = response.data[i].offer.offerQuantity

                }
                this.setState({ data: response.data, currentUser: currentUser, userReady: true, isLoading: false})
                break
            }
        }

    }


    handleRemove = i => {
        this.setState(state => ({
          data: state.data.filter((row, j) => j !== i)
        }));
      };
    
    handleAdd = () => {
        window.location = '/addOrder'

    }
    
    startEditing = i => {
    this.setState({ editIdx: i });
    };

    stopEditing = () => {
    this.setState({ editIdx: -1 });
    };

    handleChange = (e, name, i) => {
        const { value } = e.target;
        this.setState(state => ({
            data: state.data.map(
            (row, j) => (j === i ? { ...row, [name]: value } : row)
            )
        }));
    };

    render() {

        const { currentUser, redirect, error, isLoading } = this.state;

        if (redirect) {
            return <Redirect to={redirect} />
        }

        if (error === true) {
            return <p>{error.message}</p>;
          }

        if (isLoading === true) {
            return <p>Loading...</p>;
        }
        return(
            <Container fluid>
                    <h1 className="text-center">
                        <span className="font-italic" style={{fontSize : '75%'}}>Užsakymai</span>
                    </h1>
                    <hr/>

                        <Row>
                            <Col xs={5} md={5} lg={2}>
                            <Button className="btn-m btn-secondary btn-block" type="submit" onClick={() => this.handleAdd()}>Pridėti užsakymą</Button>
                            </Col>
                        </Row>
                        <br></br>
                        <Row>
                            <Col>
                            <MuiThemeProvider>
                                <Table
                                    handleRemove={this.handleRemove}
                                    startEditing={this.startEditing}
                                    editIdx={this.state.editIdx}
                                    stopEditing={this.stopEditing}
                                    handleChange={this.handleChange}
                                    data={this.state.data}
                                    header={[
                                    {
                                        name: "Klientas",
                                        prop: "customerCompanyName"
                                    },
                                    {
                                        name: "Suma Eur.",
                                        prop: "salesVATInvoicePayAmountAfterVAT"
                                    },
                                    {
                                        name: "Užsakymo kiekis",
                                        prop: "offerQuantity"
                                    },
                                    {
                                        name: "Užsakymo data",
                                        prop: "salesVATInvoiceVATDate"
                                    }, 
                                    {
                                        name: "Sumokėti iki data",
                                        prop: "salesVATInvoiceAmountPayTill"
                                    },
                                    ]}
                                />
                            </MuiThemeProvider>
                            </Col>
                        </Row>
            </Container>
        )
    }
}
export default OrdersPage;