import React, { Component } from 'react'
import { Container, Button, Form} from 'react-bootstrap'
import Select from 'react-select'
import DatePicker from 'react-datepicker';
import { Redirect } from "react-router-dom";

import "react-datepicker/dist/react-datepicker.css";
import 'bootstrap/dist/css/bootstrap.min.css';
import AuthService from '../services/auth.service'
import CountryService from '../services/country.service'
import CustomerPaymentStatusService from '../services/customePaymentStatus.services'
import CustomerService from '../services/customers.service'

class AddClientPage extends Component {
    constructor(props) {
        super(props)

        this.state = {
            redirect: null,
            userReady: false,
            currentUser: { userEmail: "" },
            data: null,
            isLoading: true,
            error: null,

            // Selections
            countrySelectionOptions: [],
            customerCountry: null, 
            customerCountryId: null,
            customerPaymentStatusSelectionOptions: [],
            customerPaymentStatus:  null, 
            customerPaymentStatusId: null,
            
            // Customer Object Data 
            customerCompanyName: null,
            customerCompanyCode: null, 
            customerAdress : null, 
            customerVATCode: null, 
            customerContactPersonName: null, 
            customerContactPersonPhoneNumber: null, 
            customerEmail: null, 
            customerDeliveryAdress1: null, 
            customerDeliveryPersonName: null, 
            customerDeliveryPersonPhoneNumber: null

        }

        this.handleChangecustomerPaymentStatus = this.handleChangecustomerPaymentStatus.bind(this);
        this.handleChangeCustomerCountry = this.handleChangeCustomerCountry.bind(this);
        this.handleChangeCustomerCompanyName = this.handleChangeCustomerCompanyName.bind(this);
        this.handleChangeCustomerCompanyCode = this.handleChangeCustomerCompanyCode.bind(this);
        this.handleChangeCustomerAdress = this.handleChangeCustomerAdress.bind(this);
        this.handleChangeCustomerVATCode = this.handleChangeCustomerVATCode.bind(this);
        this.handleChangeCustomerContactPersonName = this.handleChangeCustomerContactPersonName.bind(this);
        this.handleChangeCustomerContactPersonPhoneNumber = this.handleChangeCustomerContactPersonPhoneNumber.bind(this);
        this.handleChangeCustomerEmail = this.handleChangeCustomerEmail.bind(this);
        this.handleChangeCustomerDeliveryAdress1 = this.handleChangeCustomerDeliveryAdress1.bind(this);
        this.handleChangeCustomerDeliveryPersonName = this.handleChangeCustomerDeliveryPersonName.bind(this);
        this.handleChangeCustomerDeliveryPersonPhoneNumber = this.handleChangeCustomerDeliveryPersonPhoneNumber.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    handleChangecustomerPaymentStatus = (e) => {
        console.log(e)
        this.setState({
            customerPaymentStatus : e.label,
            customerPaymentStatusId: e.value
        })
    }
    handleChangeCustomerCountry = (e) => {
        console.log(e)
        this.setState({
            customerCountry : e.label,
            customerCountryId: e.value
        })
    }

    handleChangeCustomerCompanyName(e) {
        this.setState({
            customerCompanyName: e.target.value
        })
    }

    handleChangeCustomerCompanyCode(e) {
        this.setState({
            customerCompanyCode: e.target.value
        })
    }

    handleChangeCustomerAdress(e) {
        this.setState({
            customerAdress: e.target.value
        })
    }

    handleChangeCustomerVATCode(e){
        this.setState({
            customerVATCode:e.target.value
        })
    }

    handleChangeCustomerContactPersonName(e) {
        this.setState({
            customerContactPersonName : e.target.value
        })
    }

    handleChangeCustomerContactPersonPhoneNumber(e){
        this.setState({
            customerContactPersonPhoneNumber : e.target.value
        })    
    }

    handleChangeCustomerEmail(e){
        this.setState({
            customerEmail : e.target.value
        })    
    }

    handleChangeCustomerDeliveryAdress1(e){
        this.setState({
            customerDeliveryAdress1 : e.target.value
        })    
    }

    handleChangeCustomerDeliveryPersonName(e){
        this.setState({
            customerDeliveryPersonName : e.target.value
        })    
    }

    handleChangeCustomerDeliveryPersonPhoneNumber(e){
        this.setState({
            customerDeliveryPersonPhoneNumber : e.target.value
        })    
    }

    async onSubmit(e) {
        e.preventDefault();
        const customer = {
            customerPaymentStatusId:  this.state.customerPaymentStatusId, 
            customerCountryId: this.state.customerCountryId, 
            customerCompanyName: this.state.customerCompanyName,
            customerCompanyCode: this.state.customerCompanyCode, 
            customerAdress : this.state.customerAdress, 
            customerVATCode: this.state.customerVATCode, 
            customerContactPersonName: this.state.customerContactPersonName, 
            customerContactPersonPhoneNumber: this.state.customerContactPersonPhoneNumber, 
            customerEmail: this.state.customerEmail, 
            customerDeliveryAdress1: this.state.customerDeliveryAdress1, 
            customerDeliveryPersonName: this.state.customerDeliveryPersonName, 
            customerDeliveryPersonPhoneNumber: this.state.customerDeliveryPersonPhoneNumber
        }

        console.log(customer);
        
        await CustomerService.addNewCustomer(customer.customerPaymentStatusId, customer.customerCountryId, customer.customerCompanyName, customer.customerCompanyCode, customer.customerAdress,
            customer.customerVATCode, customer.customerContactPersonName, customer.customerContactPersonPhoneNumber, customer.customerEmail,
            customer.customerDeliveryAdress1, customer.customerDeliveryPersonName, customer.customerDeliveryPersonPhoneNumber)
            .then(res=> console.log(res.data))
            .catch(err=> console.log(err))
        
        this.setState({
            countrySelectionOptions: [],
            customerPaymentStatusSelectionOptions: [],
            customerPaymentStatus:  null, 
            customerCountry: null, 
            customerCompanyName: null,
            customerCompanyCode: null, 
            customerAdress : null, 
            customerVATCode: null, 
            customerContactPersonName: null, 
            customerContactPersonPhoneNumber: null, 
            customerEmail: null, 
            customerDeliveryAdress1: null, 
            customerDeliveryPersonName: null, 
            customerDeliveryPersonPhoneNumber: null
        });
        alert("Naujas klientas sėkmingai pridėtas!")
        window.location.reload();
    }


    async componentDidMount() {
        this.setState({ data: this.state.usersData, isLoading: true})
        const currentUser =  await AuthService.getCurrentUser();

        if (!currentUser)
        {
            this.setState({ redirect: "/login" });
        }
        this.setState({ currentUser: currentUser, userReady: true })

        await this.getAllCountries();
        await this.getCustomerPaymentStatuses();
    }


    async getAllCountries(){
        const res = await CountryService.getAllCountries();
        const data = res.data
        const options = data.map(d => ({
          "label" : d.countryName,
          "value" : d._id
    
        }))

        this.setState({countrySelectionOptions: options})
    }

    async getCustomerPaymentStatuses(){
        const res = await CustomerPaymentStatusService.getAllCustomerPaymentStatuses();
        const data = res.data
        const options = data.map(d => ({
          "label" : d.customerPaymentStatusName,
          "value" : d._id
    
        }))

        this.setState({customerPaymentStatusSelectionOptions: options, isLoading: false})
    }

    render() {
        
        const { currentUser, redirect, error, isLoading } = this.state;


        if (redirect) {
            return <Redirect to={redirect} />
        }

        if (error === true) {
            return <p>{error.message}</p>;
          }

        if (isLoading === true) {
            return <p>Loading...</p>;
        }
      
        
        return(
            <Container>
                    <h1 className="text-center">
                        <span className="font-italic" style={{fontSize : '75%'}}>Pridėkite naują kliento informaciją</span>
                    </h1>
                    <hr/>

                    <Form onSubmit={this.onSubmit}>
                
                        <Form.Group>
                            <Form.Label>Kliento mokėjimo statusas*</Form.Label>
                            <Select options={this.state.customerPaymentStatusSelectionOptions} onChange={this.handleChangecustomerPaymentStatus} placeholder="Pasirinkite kliento mokėjimo statusą"/>
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Šalis*</Form.Label>
                            <Select options={this.state.countrySelectionOptions} onChange={this.handleChangeCustomerCountry} placeholder="Pasirinkite šalį"/>
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Įmonės kodas*</Form.Label>
                            <Form.Control placeholder="Įveskite įmonės kodą" onChange={this.handleChangeCustomerCompanyCode}/>
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Įmonės pavadinimas*</Form.Label>
                            <Form.Control placeholder="Įveskite įmonės pavadinimą" onChange={this.handleChangeCustomerCompanyName}/>
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Įmonės adresas*</Form.Label>
                            <Form.Control placeholder="Įveskite įmonės adresą" onChange={this.handleChangeCustomerAdress}/>
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>PVM mokėtojo kodas*</Form.Label>
                            <Form.Control placeholder="Įveskite PVM mokėtojo kodą" onChange={this.handleChangeCustomerVATCode}/>
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Kontaktinio asmens vardas*</Form.Label>
                            <Form.Control placeholder="Įveskite kontaktinio asmens vardą" onChange={this.handleChangeCustomerContactPersonName}/>
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Kontaktinio asmens telefono numeris*</Form.Label>
                            <Form.Control placeholder="Įveskite darbuotojo telefono numerį" onChange={this.handleChangeCustomerContactPersonPhoneNumber}/>
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Elektroninis paštas*</Form.Label>
                            <Form.Control placeholder="Įveskite elektroninį paštą" onChange={this.handleChangeCustomerEmail}/>
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Pristatymo adresas*</Form.Label>
                            <Form.Control placeholder="Įveskite pristatymo adresą" onChange={this.handleChangeCustomerDeliveryAdress1}/>
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Pristatymą priimančio asmens vardas*</Form.Label>
                            <Form.Control placeholder="Įveskite pristatymą priimančio asmens vardą" onChange={this.handleChangeCustomerDeliveryPersonName}/>
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Pristatymą priimančio asmens telefono numeris*</Form.Label>
                            <Form.Control placeholder="Įveskite pristatymą priimančio asmens telefono numerį" onChange={this.handleChangeCustomerDeliveryPersonPhoneNumber}/>
                        </Form.Group>

                        <Button variant="secondary" type="submit">
                            Pridėti
                        </Button>
                    </Form>
                     
            </Container>
        )
    }
}
export default AddClientPage;