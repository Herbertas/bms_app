import React, { Component } from 'react'
import { Container, Row, Col, Button } from 'react-bootstrap'
import NavBar from '../components/NavBar'
import Table from '../components/Table'
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import ProductService from '../services/product.service';
import AuthService from '../services/auth.service'; 
import { Redirect } from "react-router-dom";

class ProductsPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            redirect: null,
            userReady: false,
            currentUser: { userEmail: "" },
            editIdx : -1,
            isLoading: true,
            error: null, 

            data: null
        };
    }

    async componentDidMount(){

        this.setState({isLoading: true});
        await ProductService.getAllProducts()
        .then(
            response => {
                console.log(response.data)
                this.setState({data: response.data});
            }

        ).catch(
            error => this.setState({
                error,
                isLoading: false
            })
        )
        const currentUser =  await AuthService.getCurrentUser();

        if (!currentUser)
        {
            this.setState({ redirect: "/login" });
        }
        this.setState({ currentUser: currentUser, userReady: true, isLoading: false})
    }


    handleRemove = i => {
        this.setState(state => ({
          data: state.data.filter((row, j) => j !== i)
        }));
      };
    
    handleAdd = () => {
        window.location = '/addProduct'

    }
    
    startEditing = i => {
    this.setState({ editIdx: i });
    };

    stopEditing = () => {
    this.setState({ editIdx: -1 });
    };

    handleChange = (e, name, i) => {
        const { value } = e.target;
        this.setState(state => ({
            data: state.data.map(
            (row, j) => (j === i ? { ...row, [name]: value } : row)
            )
        }));
    };

    render() {

        const { currentUser, redirect, error, isLoading } = this.state;

        if (redirect) {
            return <Redirect to={redirect} />
        }

        if (error === true) {
            return <p>{error.message}</p>;
          }

        if (isLoading === true) {
            return <p>Loading...</p>;
        }
        return(
            <Container fluid>
                    <h1 className="text-center">
                        <span className="font-italic" style={{fontSize : '75%'}}>Produktai</span>
                    </h1>
                    <hr/>

                        <Row>
                            <Col xs={5} md={5} lg={2}>
                            <Button className="btn-m btn-secondary btn-block" type="submit" onClick={() => this.handleAdd()}>Pridėti produktą</Button>
                            </Col>
                        </Row>
                        <br></br>
                        <Row>
                            <Col>
                            <MuiThemeProvider>
                                <Table
                                    handleRemove={this.handleRemove}
                                    startEditing={this.startEditing}
                                    editIdx={this.state.editIdx}
                                    stopEditing={this.stopEditing}
                                    handleChange={this.handleChange}
                                    data={this.state.data}
                                    header={[
                                    {
                                        name: "Kodas",
                                        prop: "productCode"
                                    },
                                    {
                                        name: "Pavadinimas",
                                        prop: "productName"
                                    },
                                    {
                                        name: "Standartinė kainą už vnt. (Eur)",
                                        prop: "productStandardPrice"
                                    }
                                    ]}
                                />
                            </MuiThemeProvider>
                            </Col>
                        </Row>
            </Container>
        )
    }
}
export default ProductsPage;