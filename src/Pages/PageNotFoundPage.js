import React, { Component } from 'react'
import {Container} from 'react-bootstrap'


class PageNotFoundPage extends Component {
    constructor(props) {
        super(props)
      }

    render() {
        return(
                <Container>
                    <h1 className="text-center">
                        <span className="font-italic">Puslapis nerastas</span>
                    </h1>
                    <h5 className="text-center">
                        <span className="font-italic">Prašome įsitikinti jog turite prieiga prie šio puslapio</span>
                    </h5>
                </Container>
        )
    }
}
export default PageNotFoundPage;