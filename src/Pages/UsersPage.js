import React, { Component } from 'react'
import { Container, Row, Col, Button } from 'react-bootstrap'
import Table from '../components/Table'
import { Redirect } from "react-router-dom";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import UserService from "../services/user.service";
import AuthService from "../services/auth.service";
import EmployeeService from "../services/employee.service";
class UsersPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            redirect: null,
            userReady: false,
            currentUser: { userEmail: "" },
            usersData: null, 
            employeeData: null, 
            data: null,
            editIdx : -1,
        };
    }

    componentDidMount = async() => {

        await EmployeeService.getAllEmployees().then(
            response => {
                // TODO: workaround Need to change
                for(var i = 0; i < response.data.length; i++)
                {
                    response.data[i].userFirstName = response.data[i].user.userFirstName;
                    response.data[i].userLastName = response.data[i].user.userLastName;
                    response.data[i].userEmail = response.data[i].user.userEmail;
                }

                console.log(response.data)
                this.setState({
                    employeeData: response.data
                })
            },
            error => {
                console.log(error)
            }
        )

        console.log(this.state.data)
        const currentUser =  await AuthService.getCurrentUser();

        if (!currentUser)
        {
            this.setState({ redirect: "/login" });
        }
        this.setState({ data: this.state.employeeData})
        this.setState({ currentUser: currentUser, userReady: true })

    }

    handleRemove = i => {
        this.setState(state => ({
          data: state.data.filter((row, j) => j !== i)
        }));
      };
    
    handleAdd = () => {
        window.location = '/addUser'
    }
    
    startEditing = i => {
    this.setState({ editIdx: i });
    };

    stopEditing = () => {
    this.setState({ editIdx: -1 });
    };

    handleChange = (e, name, i) => {
        const { value } = e.target;
        this.setState(state => ({
            data: state.data.map(
            (row, j) => (j === i ? { ...row, [name]: value } : row)
            )
        }));

        // TODO: Update Axios procedure
    };

    render() {

        if (this.state.redirect) {
            return <Redirect to={this.state.redirect} />
        }
      
        const { currentUser } = this.state;

        return(
            <Container fluid>
                {(this.state.userReady) ?
                (
                    <div>
                    <h1 className="text-center">
                        <span className="font-italic" style={{fontSize : '75%'}}>Sistemos naudotojai</span>
                    </h1>
                    <hr/>
                    <Row>
                        <Col xs={5} md={5} lg={2}>
                        <Button className="btn-m btn-secondary btn-block" type="submit" onClick={() => this.handleAdd()}>Pridėti naudotoją</Button>
                        </Col>
                    </Row>
                    <br></br>
                    <Row>
                        <Col>
                        <MuiThemeProvider>
                            <Table
                                width={'100%'}
                                handleRemove={this.handleRemove}
                                startEditing={this.startEditing}
                                editIdx={this.state.editIdx}
                                stopEditing={this.stopEditing}
                                handleChange={this.handleChange}
                                data={this.state.data}
                                header={[
                                {
                                    name: "Vardas",
                                    prop: "userFirstName"
                                },
                                {
                                    name: "Pavardė",
                                    prop: "userLastName"
                                },
                                {
                                    name: "El. paštas",
                                    prop: "userEmail"
                                }, 
                                {
                                    name: "Telefono numeris",
                                    prop: "employeePhoneNumber"
                                }, 
                                {
                                    name: "Adresas", 
                                    prop: "employeeAdress"
                                }
                                ]}
                            />
                            </MuiThemeProvider>
                        </Col>
                    </Row>
                    </div>
                )
                :
                null
                }
            </Container>
        )
    }
}
export default UsersPage;