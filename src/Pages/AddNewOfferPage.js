import React, { Component } from 'react'
import { Container, Button, Form } from 'react-bootstrap'
import { Redirect } from "react-router-dom";
import { ThemeConsumer } from 'react-bootstrap/esm/ThemeProvider';
import { form } from 'react-validation/build/form';
import Select from 'react-select'
import DatePicker from 'react-datepicker';

import ProductService from '../services/product.service'
import AuthService from '../services/auth.service'
import OffersService from '../services/offers.service'
import CustomerService from '../services/customers.service'
import EmployeeService from '../services/employee.service'
import CurrencyInput from 'react-currency-input-field';


class AddNewOfferPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            redirect: null,
            userReady: false,
            currentUser: { userEmail: "" },
            data: null,
            isLoading: false,
            error: null,

            employeeId: null,
            productSelectionOptions: [],
            selectedProductCode: null,
            selectedProductId: null, 

            customerSelectionOptions: [], 
            selectedCustomerName: null, 
            selectedCustomerId: null,

            offerQuantity: null, 
            offerValidFromDate: null, 
            offerValidTillDate: null, 
            offerPrice: null,

            formError : {
                offerPrice : null,
                selectedCustomerId: null, 
                selectedProductId: null,
                offerValidFromDate: null, 
                offerValidTillDate: null, 
                offerQuantity: null

            },

            formErrorFound : true
        }
         this.handleChangeOfferQuantity = this.handleChangeOfferQuantity.bind(this)
         this.handleChangeOfferPrice = this.handleChangeOfferPrice.bind(this)
         this.handleChangeProduct = this.handleChangeProduct.bind(this)
         this.handleChangeCustomer = this.handleChangeCustomer.bind(this)
         this.handleChangeOfferValidFromDate = this.handleChangeOfferValidFromDate.bind(this)
         this.handleChangeOfferValidTillDate = this.handleChangeOfferValidTillDate.bind(this)
         this.onSubmit = this.onSubmit.bind(this);


    }

    findSomeErrors = async() => {
        const {formError, offerPrice, selectedCustomerId, selectedProductId, offerValidFromDate, offerValidTillDate, offerQuantity} = this.state;

        if (offerPrice === null) formError.offerPrice = "Šis laukelis privalomas"
        else formError.offerPrice = null

        if (selectedCustomerId === null) formError.selectedCustomerId = "Šis laukelis privalomas"
        else formError.selectedCustomerId = null

        if (selectedProductId === null) formError.selectedProductId = "Šis laukelis privalomas"
        else formError.selectedProductId = null

        if (offerValidFromDate === null) formError.offerValidFromDate = "Šis laukelis privalomas"
        else formError.offerValidFromDate = null

        if (offerValidTillDate === null) formError.offerValidTillDate = "Šis laukelis privalomas"
        else formError.offerValidTillDate = null

        if (offerQuantity === null) formError.offerQuantity = "Šis laukelis privalomas"
        else formError.offerQuantity = null

        if(formError.offerPrice || formError.selectedCustomerId || formError.selectedProductId || formError.offerValidFromDate || formError.offerValidTillDate || formError.offerQuantity)
        {
            this.setState({formErrorFound: true})
        }
        else
        {
            this.setState({formErrorFound: false})
        }


    }

    handleChangeOfferQuantity = (e) => {
        this.setState({
            offerQuantity : e.target.value
        })
    }

    handleChangeOfferPrice = (e) => {
        this.setState({
            offerPrice : e.target.value
        })
    }

    handleChangeProduct = (e) => {
        this.setState({
            selectedProductCode: e.label,
            selectedProductId: e.value
        })
    }

    handleChangeCustomer = (e) => {
        this.setState({
            selectedCustomerName: e.label,
            selectedCustomerId: e.value
        })
    }

    handleChangeOfferValidFromDate = (e) => {
        this.setState({
            offerValidFromDate: e,
        })
    }

    handleChangeOfferValidTillDate = (e) => {
        this.setState({
            offerValidTillDate: e,
        })
    }

    async getEmployeeData() {
        const res = await EmployeeService.getEmployeeByUserId(this.state.currentUser.id)
        console.log(res)
        this.setState({employeeId: res.data._id})
    }

    async onSubmit(e) {
        e.preventDefault();

        const {selectedCustomerId, employeeId, selectedProductId, offerQuantity, offerValidFromDate, offerValidTillDate, offerPrice} = this.state
        await this.findSomeErrors();
     
        if(this.state.formErrorFound === false)
        {
    
            await OffersService.addNewOffer(selectedCustomerId, employeeId, selectedProductId,
                offerQuantity, offerValidFromDate, offerValidTillDate, offerPrice).then(
                    () => {
                        alert("Naujas pasiūlymas sėkmingai pridėtas!")
                        window.location.reload();
                    },
                    error => {
                      const resMessage =
                        (error.response &&
                          error.response.data &&
                          error.response.data.message) ||
                        error.message ||
                        error.toString();
            
                      this.setState({
                        loading: false,
                        message: resMessage
                      });
                    }
                );
        }

    
    }


    async componentDidMount() {
        this.setState({ data: this.state.usersData, isLoading: true})
        const currentUser =  await AuthService.getCurrentUser();

        if (!currentUser)
        {
            this.setState({ redirect: "/login" });
        }
        if(!(currentUser.userRoles.includes("ROLE_ADMIN") || currentUser.userRoles.includes("ROLE_SALESMAN")))
        {
            this.setState({redirect: "/home" })
        }
        this.setState({ currentUser: currentUser, userReady: true, isLoading: false})
        await this.getCustomers();
        await this.getAllProducts();
        await this.getEmployeeData();


        this.setState({isLoading: false})
    }

    async getCustomers()
    {
        const res = await CustomerService.getAllCustomers()
        const data = res.data
        console.log(data)
        const options = data.map(d => ({
            "label" : d.customerCompanyName,
            "value" : d._id
      
          }))
  
          this.setState({customerSelectionOptions: options})
    }

    async getAllProducts()
    {
        const res = await ProductService.getAllProducts()
        const data = res.data
        const options = data.map(d => ({
            "label" : d.productCode,
            "value" : d._id
      
        }))
  
        this.setState({productSelectionOptions: options})
    }



    render() {

        const { currentUser, redirect, error, isLoading } = this.state;


        if (redirect) {
            return <Redirect to={redirect} />
        }

        if (error === true) {
            return <p>{error.message}</p>;
          }

        if (isLoading === true) {
            return <p>Loading...</p>;
        }
        return(
            <Container>
                    <h1 className="text-center">
                        <span className="font-italic" style={{fontSize : '75%'}}>Pridėkite naują pasiūlymą</span>
                    </h1>
                    <hr/>

                    <Form onSubmit={this.onSubmit}>

                        <Form.Group>
                            <Form.Label>Klientas*</Form.Label>
                            <Select options={this.state.customerSelectionOptions} onChange={this.handleChangeCustomer} placeholder="Pasirinkite klientą"/>
                            <Form.Control.Feedback type='selected' style={{color: "#dc3545", fontSize: '80%'}}>{this.state.formError.selectedCustomerId}</Form.Control.Feedback>
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Produktas*</Form.Label>
                            <Select options={this.state.productSelectionOptions} onChange={this.handleChangeProduct} placeholder="Pasirinkite produktą"/>
                            <Form.Control.Feedback type='selected' style={{color: "#dc3545", fontSize: '80%'}}>{this.state.formError.selectedProductId}</Form.Control.Feedback>
                        </Form.Group>
                
                        <Form.Group>
                            <Form.Label>Pasiūlymo kiekis vnt.*</Form.Label>
                            <Form.Control placeholder="Įveskite kiekį" onChange={this.handleChangeOfferQuantity} isInvalid={this.state.formError.offerQuantity}/>
                            {(this.state.formError.offerQuantity) ?
                                <Form.Control.Feedback type='invalid'>{this.state.formError.offerQuantity}</Form.Control.Feedback>
                                : 
                                null
                            }
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Pasiūlymo kaina Eur.*</Form.Label>
                            <br></br>
                            <CurrencyInput
                              placeholder="Įveskitę produkto kainą"
                              decimalsLimit={2}
                              suffix={"€"}
                              onValueChange={(value) => {
                                  this.setState({
                                    offerPrice : value
                                  })
                              }}>     
                            </CurrencyInput>
                            <Form.Control.Feedback type='selected' style={{color: "#dc3545", fontSize: '80%'}}>{this.state.formError.offerPrice}</Form.Control.Feedback>
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Pasiūlymas galioja nuo: *</Form.Label>
                            <br/>
                            <DatePicker
                                selected={ this.state.offerValidFromDate }
                                onChange={ this.handleChangeOfferValidFromDate}
                                name="offerValidFromDate"
                                dateFormat="MM/dd/yyyy"
                                placeholderText="Pasirinkite datą"
                            />
                            <Form.Control.Feedback type='selected' style={{color: "#dc3545", fontSize: '80%'}}>{this.state.formError.offerValidFromDate}</Form.Control.Feedback>
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Pasiūlymas galioja iki: *</Form.Label>
                            <br/>
                            <DatePicker
                                selected={ this.state.offerValidTillDate }
                                onChange={ this.handleChangeOfferValidTillDate}
                                name="offerValidTillDate"
                                dateFormat="MM/dd/yyyy"
                                placeholderText="Pasirinkite datą"
                            />
                            <Form.Control.Feedback type='selected' style={{color: "#dc3545", fontSize: '80%'}}>{this.state.formError.offerValidTillDate}</Form.Control.Feedback>
                        </Form.Group>

                        <Button variant="secondary" type="submit">
                            Pridėti
                        </Button>
                    </Form>
                     
            </Container>
        )
    }
}
export default AddNewOfferPage;