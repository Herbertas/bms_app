import React, { Component } from 'react'
import { Container, Button, Form, Row, Col } from 'react-bootstrap'
import ProductService from '../services/product.service'
import AuthService from '../services/auth.service'
import { Redirect } from "react-router-dom";
import { ThemeConsumer } from 'react-bootstrap/esm/ThemeProvider';
import { form } from 'react-validation/build/form';
import CustomerService from '../services/customers.service';
import Select from 'react-select'
import AddNewOrderPage from './AddNewOrderPage';
import ClientsPage from './ClientsPage';
import offersService from '../services/offers.service';
import Table from '../components/Table'
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";

class AddNewOrderCustomerSelectionPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            redirect: null,
            userReady: false,
            currentUser: { userEmail: "" },
            data: null,
            isLoading: false,
            error: null,
            customerSelected: false,
            offerSelected: false,

            customerSelectionOptions: [], 
            selectedCustomerId: null,
            offerSelectedData: null,


        }

         this.onSubmit = this.onSubmit.bind(this);


    }
    
    startEditing = i => {
    this.setState({ editIdx: i, offerSelectedData: this.state.data[i]});
    };

    stopEditing = () => {
    this.setState({ editIdx: -1 });
    };

    handleChange = (e, name, i) => {
        const { value } = e.target;
        this.setState(state => ({
            data: state.data.map(
            (row, j) => (j === i ? { ...row, [name]: value } : row)
            )
        }));
    };

    handleChangeCustomer = async(e) => {
        this.setState({
            selectedCustomerId : e.value, 
            selectedCustomerCompanyName: e.label
        })

        await this.getOffersByCustomerId();
    }

    async onSubmit(e) {
        e.preventDefault();
        if(this.state.offerSelectedData != null)
        {
            this.setState({
                offerSelected: true
            })
        }

    }

    async getAllCustomers(){
        const res = await CustomerService.getAllCustomers();
        const data = res.data
        const options = data.map(d => ({
          "label" : d.customerCompanyName,
          "value" : d._id
    
        }))

        this.setState({customerSelectionOptions: options})
    }

    async getOffersByCustomerId()
    {
        this.setState({customerSelected: false, data: null})

        for (var i = 0; i < 5; i++)
        {
            var res = await offersService.getOfferByCustomerId(this.state.selectedCustomerId)
            if(res.data.productCode)
            {
                break;
            }
        }

        const data = res.data

        for( var i = 0; i < data.length; i++)
        {
            data[i].productCode = data[i].product.productCode;
            data[i].productName = data[i].product.productName; 
            data[i].offerValidFromDate = new Date(data[i].offerValidFromDate).toLocaleDateString();
            data[i].offerValidTillDate = new Date(data[i].offerValidTillDate).toLocaleDateString();

        }

        this.setState({data: data, customerSelected: true})
    }

    async componentDidMount() {
        this.setState({ data: this.state.usersData, isLoading: true})
        const currentUser =  await AuthService.getCurrentUser();

        if (!currentUser)
        {
            this.setState({ redirect: "/login" });
        }

        await this.getAllCustomers();

        this.setState({ currentUser: currentUser, userReady: true, isLoading: false})
    }

    render() {

        const { offerSelected, customerSelected, currentUser, redirect, error, isLoading, } = this.state;


        if (redirect) {
            return <Redirect to={redirect} />
        }

        if (error === true) {
            return <p>{error.message}</p>;
          }

        if (isLoading === true) {
            return <p>Loading...</p>;
        }

        if(customerSelected && offerSelected)
        {
            return <AddNewOrderPage offer={this.state.offerSelectedData}></AddNewOrderPage>
        }
        return(
            <Container fluid>
                <h1 className="text-center">
                    <span className="font-italic" style={{fontSize : '75%'}}>Pasirinkite kliento pasiūlymus</span>
                </h1>
                <hr/>

                <Form onSubmit={this.onSubmit}>

                    <Row>
                        <Col>
                            <Form.Group>
                                    <Form.Label>Klientas</Form.Label>
                                    <Select options={this.state.customerSelectionOptions} onChange={this.handleChangeCustomer} placeholder="Pasirinkite klientą"/>
                            </Form.Group>
                        </Col>
                        <Col></Col>
                        <Col></Col>
                    </Row>
                    <Row>
                        {(this.state.customerSelected) ? 
                        <Col>
                            {(this.state.data != null) ?
                            <MuiThemeProvider>

                                <Table
                                    handleRemove={null}
                                    startEditing={this.startEditing}
                                    editIdx={this.state.editIdx}
                                    stopEditing={this.stopEditing}
                                    handleChange={this.setState.handleChange}
                                    data={this.state.data}
                                    selectable={true}
                                    header={[
                                    {
                                        name: "Produkto kodas",
                                        prop: "productCode"
                                    },
                                    {
                                        name: "Produkto pavadinimas",
                                        prop: "productName"
                                    },
                                    {
                                        name: "Užsakomas kiekis vnt.",
                                        prop: "offerQuantity"
                                    },
                                    {
                                        name: "Vieneto kainą",
                                        prop: "offerPrice"
                                    }, 
                                    {
                                        name: "Galioja nuo",
                                        prop: "offerValidFromDate"
                                    }, 
                                    {
                                        name: "Galioja iki", 
                                        prop: "offerValidTillDate"
                                    }
                                    ]}
                                />
                            </MuiThemeProvider>
                            :
                            <Form.Label>Pasiūlymų nerasta</Form.Label>
                            }
                        </Col>
                        :
                        null
                        }
                    </Row>
                    <Row>
                        <Col>
                            <Button variant="secondary" type="submit" style={{marginTop: "10px"}}>
                                Pasirinkti
                            </Button>
                        </Col>
                    </Row>

                </Form>
                     
            </Container>
        )
    }
}
export default AddNewOrderCustomerSelectionPage;