import React, { Component } from 'react'
import { Container, Row, Col } from 'react-bootstrap'
import NavBar from '../components/NavBar'
import Table from '../components/Table'
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import AuthService from '../services/auth.service'
import { Redirect } from "react-router-dom";

class HomePage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            redirect: null,
            userReady: false,
            currentUser: { userEmail: "" },
            data: [{clientName: "UAB ss", orderStatus: "Herbertas", deliveryDate: "2020-10-10", invoiceDocument: "Herbertas"}, 
            {clientName: "UAB ls", orderStatus: "Herbertas", deliveryDate: "2020-10-10", invoiceDocument: "Herbertas"}],
            editIdx : -1,
            isLoading: true,
            error: null
        }
    }

    async componentDidMount() {

        this.setState({isLoading: true});
        const currentUser =  await AuthService.getCurrentUser();

        if (!currentUser)
        {
            this.setState({ redirect: "/login" });
        }
        this.setState({ currentUser: currentUser, userReady: true, isLoading: false})
        
    }

    handleRemove = i => {
        this.setState(state => ({
          data: state.data.filter((row, j) => j !== i)
        }));
      };
    
    startEditing = i => {
    this.setState({ editIdx: i });
    };

    stopEditing = () => {
    this.setState({ editIdx: -1 });
    };

    handleChange = (e, name, i) => {
        const { value } = e.target;
        this.setState(state => ({
            data: state.data.map(
            (row, j) => (j === i ? { ...row, [name]: value } : row)
            )
        }));
    };

    render() {

        const { currentUser, redirect, error, isLoading } = this.state;

        if (redirect) {
            return <Redirect to={redirect} />
        }

        if (error === true) {
            return <p>{error.message}</p>;
          }

        if (isLoading === true) {
            return <p>Loading...</p>;
        }

        return(
            <Container fluid>
            <h1 className="text-center">
                <span className="font-italic" style={{fontSize : '75%'}}>Užsakymai</span>
            </h1>
            <hr/>
                <Row>
                    <Col>
                    <MuiThemeProvider>

                        <Table
                            handleRemove={this.handleRemove}
                            startEditing={this.startEditing}
                            editIdx={this.state.editIdx}
                            stopEditing={this.stopEditing}
                            handleChange={this.handleChange}
                            data={this.state.data}
                            header={[
                            {
                                name: "Įmonės pavadinimas",
                                prop: "clientName"
                            },
                            {
                                name: "Užsakymo būsena",
                                prop: "orderStatus"
                            },
                            {
                                name: "Pristatymo data",
                                prop: "deliveryDate"
                            },
                            {
                                name: "Sąskaita faktūra",
                                prop: "invoiceDocument"
                            }
                            ]}
                        />
                    </MuiThemeProvider>
                    </Col>
                </Row>
            </Container>
        )
    }
}
export default HomePage;