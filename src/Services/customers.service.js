import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'http://localhost:5000/customers/';

class CustomerService {

  async getAllCustomers(){
    const result = await axios.get(API_URL, { headers: authHeader()});
    console.log(result.data)
    console.log("AAAAAA")
    return result;
  }

  addNewCustomer(customerPaymentStatusId, countryId, customerCompanyName, customerCompanyCode, customerAdress, customerVATCode,
    customerContactPersonName, customerContactPersonPhoneNumber, customerEmail, customerDeliveryAdress1, customerDeliveryPersonName, customerDeliveryPersonPhoneNumber) {
    
    return  axios.post(API_URL + 'add',   
    {
      customerPaymentStatusId: customerPaymentStatusId,
      countryId: countryId,
      customerCompanyName: customerCompanyName,
      customerCompanyCode: customerCompanyCode,
      customerAdress: customerAdress,
      customerVATCode: customerVATCode,
      customerContactPersonName: customerContactPersonName,
      customerContactPersonPhoneNumber: customerContactPersonPhoneNumber,
      customerEmail: customerEmail,
      customerDeliveryAdress1: customerDeliveryAdress1,
      customerDeliveryPersonName: customerDeliveryPersonName,
      customerDeliveryPersonPhoneNumber: customerDeliveryPersonPhoneNumber
    },
    {headers: authHeader()});
  }

  
  updateCustomer(data) {
    return axios.put(API_URL)
  }
}

export default new CustomerService();