import axios from "axios";
import authHeader from './auth-header';

const API_URL = "http://localhost:5000/productFaultStatus/";

class ProductFaultStatusService {

    getAllProductFaultStatuses = async() => {
        const response = await axios.get(API_URL, { headers: authHeader() })
        console.log(response.data);
        return response;
    }

}

export default new ProductFaultStatusService();