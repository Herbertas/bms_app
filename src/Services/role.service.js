import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'http://localhost:5000/roles/';

class RoleService {

  getAllRoles = async() => {
    const result = await axios.get(API_URL, { headers: authHeader()});
    console.log(result.data)
    return result;
  }

}

export default new RoleService();