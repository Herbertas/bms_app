import axios from "axios";
import authHeader from './auth-header';

const API_URL = "http://localhost:5000/orderStatuses/";

class OrderStatusService {

    getAllOrderStatuses = async() => {
        const response = await axios.get(API_URL, { headers: authHeader() })
        return response;
    }

}

export default new OrderStatusService();