import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'http://localhost:5000/users/';
const ADD_USER_API = 'http://localhost:5000/api/auth/signup'

class UserService {

  getAllUsers = async() => {
    const result = await axios.get(API_URL, { headers: authHeader()});
    console.log(result.data)
    return result;
  }


  addNewUser(data) {
    return  axios.post(ADD_USER_API, data, {headers: authHeader()});
  }

  addUpdateUser(data) {
    return axios.put(API_URL)
  }
}

export default new UserService();