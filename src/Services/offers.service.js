import axios from "axios";
import authHeader from './auth-header';

const API_URL = "http://localhost:5000/offers/";

class OffersService {

    getAllOffers = async() => {
        const response = await axios.get(API_URL, { headers: authHeader() })
        return response;
    }

    async addNewOffer(customerId, employeeId, productId, offerQuantity, offerValidFromDate, offerValidTillDate, offerPrice) {
        await axios.post(API_URL + 'addNewOffer',{
            customerId: customerId, 
            employeeId: employeeId, 
            productId: productId, 
            offerQuantity: offerQuantity, 
            offerValidFromDate: offerValidFromDate,
            offerValidTillDate: offerValidTillDate,
            offerPrice: offerPrice
        }, {headers: authHeader()})
    }

    async getOfferByCustomerId(customerId)
    {
        const response = await axios.get(API_URL + 'byCustomerId', {params: {customerId: customerId}, headers: authHeader()})
        return response
    }

}

export default new OffersService();