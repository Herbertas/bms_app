import axios from "axios";
import authHeader from './auth-header';

const API_URL = "http://localhost:5000/productFault/";

class ProductFaultService {

    getAllProductFaults = async() => {
        const response = await axios.get(API_URL, { headers: authHeader() })
        console.log(response.data);
        return response;
    }

    addProductFault(productFaultProductId, productFaultProductFaultStatusId, productFaultCustomerId, productFaultDescription, productFaultEmployeeId) {
        
        return axios.post(API_URL + 'addProductFault', 
        {
            productFaultProductId: productFaultProductId, 
            productFaultProductFaultStatusId: productFaultProductFaultStatusId,
            productFaultCustomerId: productFaultCustomerId, 
            productFaultDescription: productFaultDescription, 
            productFaultEmployeeId: productFaultEmployeeId
        }, 
        { headers: authHeader() })
    }

}

export default new ProductFaultService();