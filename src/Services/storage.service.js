import axios from 'axios';
import authHeader from './auth-header';

const API_STORAGE_URL = 'http://localhost:5000/storage/';
const API_STORED_UNIT_URL = 'http://localhost:5000/storedUnit/'

class StorageService {

  getAllStorages = async() => {
    const result = await axios.get(API_STORAGE_URL, { headers: authHeader()});
    return result;
  }

  async addNewStoredUnitToParticularStorage(productId, storageId, storedUnitProductQuantity, storedUnitProductOrderedQuantity,
    storedUnitProductManufacturingQuantity, storedUnitProductDefectiveQuantity){
      await axios.post(API_STORED_UNIT_URL + 'add', {
        productId: productId, 
        storageId: storageId, 
        storedUnitProductQuantity: storedUnitProductQuantity, 
        storedUnitProductOrderedQuantity: storedUnitProductOrderedQuantity, 
        storedUnitProductManufacturingQuantity: storedUnitProductManufacturingQuantity, 
        storedUnitProductDefectiveQuantity: storedUnitProductDefectiveQuantity
      })
  }

  async getAllProductsFromParticularStorage(storageId){
      console.log(storageId)
      const result = await axios.get(API_STORED_UNIT_URL + 'getAllStoredUnitsByStorageId', {params: {storageId: storageId}, headers: authHeader()})
      return result;
  }

}

export default new StorageService();