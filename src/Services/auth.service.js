import axios from "axios";
import {useHistory} from 'react-router-dom'
const API_URL = "http://localhost:5000/api/auth/";

class AuthService {
  login(userEmail, userPassword) {
    return axios
      .post(API_URL + "signin", {
        userEmail,
        userPassword
      })
      .then(response => {
        if (response.data.userAccessToken) {
          localStorage.setItem("user", JSON.stringify(response.data));
        }

        return response.data;
      });
  }
  logout() {
    localStorage.removeItem("user");

  }

  register(userFirstName, userLastName, userEmail, userPassword, userRoles, employeeHiredDate, employeeAdress, employeePhoneNumber
    ) {

      return axios.post(API_URL + "signup", {
      userFirstName: userFirstName, 
      userLastName: userLastName, 
      userEmail : userEmail, 
      userPassword: userPassword, 
      userRoles: userRoles, 
      employeeHiredDate: employeeHiredDate, 
      employeeAdress: employeeAdress,
      employeePhoneNumber: employeePhoneNumber
    });
  }

  getCurrentUser() {
    return JSON.parse(localStorage.getItem('user'));;
  }
}

export default new AuthService();