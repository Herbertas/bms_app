import axios from "axios";
import authHeader from './auth-header';

const API_URL = "http://localhost:5000/orders/";

class OrderService {

    getAllOrders = async() => {
        const response = await axios.get(API_URL, { headers: authHeader() })
        return response;
    }

    async addNewOrderSalesVATInvoiceConsigmentNote(consigmentNoteDate, consigmentNoteWeight, consigmentNoteVolume, customerId,
        employeeId, companyId, salesVATInvoiceVATDate, salesVATInvoicePayAmountBeforeVAT, salesVATInvoiceAmountToPayBeforeVATInWords,
        salesVATInvoiceVATAmount, salesVATInvoicePayAmountAfterVAT, salesVATInvoiceAmountToPayAfterVATInWords, salesVATInvoiceAmountPayTill,
        orderStatusId, offerId, orderDate, orderTillDate) {
            
        await axios.post(API_URL + 'addOrderSalesVATInvoiceConsigmentNote',{
            consigmentNoteDate: consigmentNoteDate, 
            consigmentNoteWeight: consigmentNoteWeight, 
            consigmentNoteVolume: consigmentNoteVolume, 
            customerId: customerId, 
            employeeId: employeeId,
            companyId: companyId,
            salesVATInvoiceVATDate: salesVATInvoiceVATDate,
            salesVATInvoicePayAmountBeforeVAT: salesVATInvoicePayAmountBeforeVAT, 
            salesVATInvoiceAmountToPayBeforeVATInWords: salesVATInvoiceAmountToPayBeforeVATInWords, 
            salesVATInvoiceVATAmount: salesVATInvoiceVATAmount, 
            salesVATInvoicePayAmountAfterVAT: salesVATInvoicePayAmountAfterVAT, 
            salesVATInvoiceAmountToPayAfterVATInWords: salesVATInvoiceAmountToPayAfterVATInWords, 
            salesVATInvoiceAmountPayTill: salesVATInvoiceAmountPayTill, 
            orderStatusId: orderStatusId,
            offerId: offerId, 
            orderDate: orderDate, 
            orderTillDate: orderTillDate
        }, {headers: authHeader()})
    }
}

export default new OrderService();