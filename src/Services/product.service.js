import axios from "axios";
import authHeader from './auth-header';

const API_URL = "http://localhost:5000/product/";

class ProductService {

    getAllProducts = async() => {
        const response = await axios.get(API_URL, { headers: authHeader() })
        console.log(response.data);
        return response;
    }

    addNewProduct(productCode, productName, productDescription, productWeight, productStandardPrice) {
        console.log(productCode, productDescription, productName, productStandardPrice, productWeight)
        return axios.post(API_URL + 'addProduct', 
        {
            productCode: productCode, 
            productName: productName,
            productDescription: productDescription, 
            productWeight: productWeight, 
            productStandardPrice: productStandardPrice
        }, 
        { headers: authHeader() })
    }

}

export default new ProductService();