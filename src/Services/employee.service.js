import axios from "axios";
import authHeader from './auth-header';

const API_URL = "http://localhost:5000/employees/";

class EmployeeService {

    getAllEmployees = async() => {
        const response = await axios.get(API_URL, { headers: authHeader() })
        return response;
    }

    async getEmployeeByUserId(userId) {
        const response = await axios.get(API_URL + 'getByUserId', {params: {userId: userId}, headers: authHeader()})
        return response;
    }

}

export default new EmployeeService();