const { authJwt } = require("../middlewares");
const controller = require("../controllers/storedUnit.controller");
const { isAdmin, isManufacturer } = require("../middlewares/authJwt");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.post(
    "/storedUnit/add", 
    [authJwt.verifyToken, isAdmin],
    controller.addNewStoredUnit
  );

  app.get(
    "/storedUnit/getAllStoredUnitsByStorageId", 
    [authJwt.verifyToken, isAdmin],
    controller.getAllStoredUnitsByStorageId
  )

};