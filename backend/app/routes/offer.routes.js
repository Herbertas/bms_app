const { authJwt } = require("../middlewares");
const controller = require("../controllers/offer.controller");


module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.get(
    "/offers/", 
    [authJwt.verifyToken], 
    controller.getAllOffers
  );

  app.post(
    "/offers/addNewOffer", 
    [authJwt.verifyToken],
    controller.addNewOffer
  );

  app.get(
    "/offers/byCustomerId", 
    [authJwt.verifyToken],
    controller.getOfferByCustomerId
  );

};