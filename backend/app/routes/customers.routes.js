const { authJwt } = require("../middlewares");
const controller = require("../controllers/customers.controller");

module.exports = function(app) {
    app.use(function(req, res, next) {
      res.header(
        "Access-Control-Allow-Headers",
        "x-access-token, Origin, Content-Type, Accept"
      );
      next();
    });

    app.get(
        "/customers/", 
        [authJwt.verifyToken], 
        controller.getCustomers
    );

    app.get(
        "/customerById/", 
        [authJwt.verifyToken],
        controller.getCustomerById
    );

    app.post(
        "/customers/add",
        [authJwt.verifyToken],
        controller.addCustomer 

    ); 

    app.put(
        "/customers/udpate", 
        [authJwt.verifyToken],
        controller.updateCustomer
    );
  
  };