const { authJwt } = require("../middlewares");
const controller = require("../controllers/productFault.controller");


module.exports = function(app) {
    app.use(function(req, res, next) {
      res.header(
        "Access-Control-Allow-Headers",
        "x-access-token, Origin, Content-Type, Accept"
      );
      next();
    });

    app.get(
        "/productFault/", 
        [authJwt.verifyToken], 
        controller.getAllProductFaults
    );  

    // app.get(
    //     "/productFault/getById/",
    //     [authJwt.verifyToken],
    //     controller.getProductFaultById 
    // ); 

    app.post(
        "/productFault/addProductFault",
        [authJwt.verifyToken], 
        controller.addProductFault
    )

    app.put(
        "/productFault/update",
        [authJwt.verifyToken],
        controller.updateProductFault 
    )
  };
