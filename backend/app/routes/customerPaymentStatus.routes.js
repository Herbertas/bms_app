const { authJwt } = require("../middlewares");
const controller = require("../controllers/customerPaymentStatus.controller");

module.exports = function(app) {
    app.use(function(req, res, next) {
      res.header(
        "Access-Control-Allow-Headers",
        "x-access-token, Origin, Content-Type, Accept"
      );
      next();
    });

    app.get(
        "/customerPaymentStatuses/", 
        [authJwt.verifyToken], 
        controller.getCustomerPaymentStatuses
    );
    
    app.get(
        "/customerPaymentStatusById/",
        [authJwt.verifyToken], 
        controller.getCustomerPaymentStatusById
    )
  };
