const { authJwt } = require("../middlewares");
const controller = require("../controllers/employee.controller");
const { isAdmin } = require("../middlewares/authJwt");


module.exports = function(app) {
    app.use(function(req, res, next) {
      res.header(
        "Access-Control-Allow-Headers",
        "x-access-token, Origin, Content-Type, Accept"
      );
      next();
    });
    
    app.get(
        "/employees/", 
        [authJwt.verifyToken, isAdmin], 
        controller.getEmployees
    );

    app.get(
        "/employees/getById/",
        [authJwt.verifyToken, isAdmin],
        controller.getEmployeeById
    );

    app.get(
        "/employees/getByUserId",
        [authJwt.verifyToken],
        controller.getEmployeeByUserId
    );

    app.post(
        "/employees/add/",
        [authJwt.verifyToken, isAdmin], 
        controller.addEmployee
    );

    app.put(
        "/employees/update/", 
        [authJwt.verifyToken, isAdmin], 
        controller.updateEmployee
    );
};