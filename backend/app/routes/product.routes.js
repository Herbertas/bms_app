const { authJwt } = require("../middlewares");
const controller = require("../controllers/product.controller");


module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.get(
    "/product/", 
    [authJwt.verifyToken], 
    controller.getProducts
  );

  app.get(
    "/product/getById", 
    [authJwt.verifyToken], 
    controller.getProductById
  );

  app.post(
    "/product/addProduct", 
    [authJwt.verifyToken],
    controller.addProduct
  );

  app.put(
    "/product/updateProduct",
    [authJwt.verifyToken],
    controller.updateProduct
  );
};