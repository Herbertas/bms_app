const { authJwt } = require("../middlewares");
const controller = require("../controllers/storage.controller");
const { isAdmin, isManufacturer } = require("../middlewares/authJwt");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.get(
    "/storage/", 
    [authJwt.verifyToken, isAdmin],
    controller.getAllStorages
  );

};