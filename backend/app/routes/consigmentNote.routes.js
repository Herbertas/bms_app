const { authJwt } = require("../middlewares");
const controller = require("../controllers/consigmentNote.controller");


module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.get(
    "/consigmentNotes/", 
    [authJwt.verifyToken], 
    controller.getAllConsigmentNotes
  );

  app.get(
    "/consigmentNotes/getById", 
    [authJwt.verifyToken],
    controller.getConsigmentNoteById
  );

};