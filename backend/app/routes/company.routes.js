const { authJwt } = require("../middlewares");
const controller = require("../controllers/company.controller");
const { isAdmin } = require("../middlewares/authJwt");

module.exports = function(app) {
    app.use(function(req, res, next) {
      res.header(
        "Access-Control-Allow-Headers",
        "x-access-token, Origin, Content-Type, Accept"
      );
      next();
    });
  
    app.post(
        "/companies/add",
        [authJwt.verifyToken, isAdmin], 
        controller.addCompany
    );

    app.get(
        "/companies/companyById", 
        [authJwt.verifyToken, isAdmin], 
        controller.getCompanyById
    )

    app.put(
        "/companies/update", 
        [authJwt.verifyToken, isAdmin], 
        controller.updateCompanyInformation
    )
    
    app.get(
        "/companies/", 
        [authJwt.verifyToken, isAdmin],
        controller.getCompanies
    )


};