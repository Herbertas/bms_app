const { authJwt } = require("../middlewares");
const { isAdmin } = require("../middlewares/authJwt");
const controller = require("../controllers/country.controller");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

/*
    Get All Countries
*/
app.get(
  "/countries/", 
  [authJwt.verifyToken], 
  controller.getCountries
);

/*
    Add New Country
*/
app.post(
  "/countries/add",
  [authJwt.verifyToken, isAdmin], 
  controller.addCountry
);

// /*
//     Get country by ID
// */
app.get(
  "/countries/countryById", 
  [authJwt.verifyToken, isAdmin], 
  controller.getCountryById
);


};
