const jwt = require("jsonwebtoken");
const config = require("../config/auth.config.js");
const { mongoose } = require("../models");
const db = require("../models");
const Employee = require("../models/employee.model.js");
const User = db.user;
const Role = db.role;

verifyToken = (req, res, next) => {
  let token = req.headers["x-access-token"];

  if (!token) {
    return res.status(403).send({ message: "No token provided!" });
  }

  jwt.verify(token, config.secret, (err, decoded) => {
    if (err) {
      return res.status(401).send({ message: "Unauthorized!" });
    }
    req.userId = decoded.id;

    next();
  });
};

isAdmin = (req, res, next) => {
  User.findById(req.userId).exec((err, user) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }

    Role.find(
      {
        _id: { $in: user.userRoles }
      },
      (err, roles) => {
        if (err) {
          res.status(500).send({ message: err });
          return;
        }

        for (let i = 0; i < roles.length; i++) {
          if (roles[i].roleName === "admin") {
            next();
            return;
          }
        }

        res.status(403).send({ message: "Require Admin Role!" });
        return;
      }
    );
  });
};

isSalesman = (req, res, next) => {
  User.findById(req.userId).exec((err, user) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }

    Role.find(
      {
        _id: { $in: user.userRoles }
      },
      (err, roles) => {
        if (err) {
          res.status(500).send({ message: err });
          return;
        }

        for (let i = 0; i < roles.length; i++) {
          if (roles[i].roleName === "salesman") {
            next();
            return;
          }
        }

        res.status(403).send({ message: "Require Salesman Role!" });
        return;
      }
    );
  });
};


isDeveloper = (req, res, next) => {
  User.findById(req.userId).exec((err, user) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }

    Role.find(
      {
        _id: { $in: user.userRoles }
      },
      (err, roles) => {
        if (err) {
          res.status(500).send({ message: err });
          return;
        }

        for (let i = 0; i < roles.length; i++) {
          if (roles[i].roleName === "developer") {
            next();
            return;
          }
        }

        res.status(403).send({ message: "Require Developer Role!" });
        return;
      }
    );
  });
};

isManufacturer = (req, res, next) => {
  User.findById(req.userId).exec((err, user) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }

    Role.find(
      {
        _id: { $in: user.userRoles }
      },
      (err, roles) => {
        if (err) {
          res.status(500).send({ message: err });
          return;
        }

        for (let i = 0; i < roles.length; i++) {
          if (roles[i].roleName === "manufacturer") {
            next();
            return;
          }
        }

        res.status(403).send({ message: "Require Manufacturer Role!" });
        return;
      }
    );
  });
};

isCustomerSupportSpecialist = (req, res, next) => {
  User.findById(req.userId).exec((err, user) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }

    Role.find(
      {
        _id: { $in: user.userRoles }
      },
      (err, roles) => {
        if (err) {
          res.status(500).send({ message: err });
          return;
        }

        for (let i = 0; i < roles.length; i++) {
          if (roles[i].roleName === "customer_support_specialist") {
            next();
            return;
          }
        }

        res.status(403).send({ message: "Require Customer Support Specialist Role!" });
        return;
      }
    );
  });
};

isAccountant = (req, res, next) => {
  User.findById(req.userId).exec((err, user) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }

    Role.find(
      {
        _id: { $in: user.userRoles }
      },
      (err, roles) => {
        if (err) {
          res.status(500).send({ message: err });
          return;
        }

        for (let i = 0; i < roles.length; i++) {
          if (roles[i].roleName === "accountant") {
            next();
            return;
          }
        }

        res.status(403).send({ message: "Require Accountant Role!" });
        return;
      }
    );
  });
};

isWarehousekeeper = (req, res, next) => {
  User.findById(req.userId).exec((err, user) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }

    Role.find(
      {
        _id: { $in: user.userRoles }
      },
      (err, roles) => {
        if (err) {
          res.status(500).send({ message: err });
          return;
        }

        for (let i = 0; i < roles.length; i++) {
          if (roles[i].roleName === "wareHouseKeeper") {
            next();
            return;
          }
        }

        res.status(403).send({ message: "Require Ware House Keeper Role!" });
        return;
      }
    );
  });
};
const authJwt = {
  verifyToken,
  isAdmin,
  isSalesman, 
  isDeveloper,
  isManufacturer, 
  isCustomerSupportSpecialist,
  isAccountant,
  isWarehousekeeper

};
module.exports = authJwt;