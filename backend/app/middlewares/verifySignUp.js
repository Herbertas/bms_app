const db = require("../models");
const ROLES = db.ROLES;
const User = db.user;

checkDuplicateEmail = (req, res, next) => {

    // Email
    User.findOne({
      userEmail: req.body.userEmail
    }).exec((err, user) => {
      if (err) {
        res.status(500).send({ message: err });
        return;
      }

      if (user) {
        res.status(400).send({ message: "Failed! Email is already in use!" });
        return;
      }

      next();
    });
};

checkRolesExisted = (req, res, next) => {
  if (req.body.userRoles) {
    for (let i = 0; i < req.body.userRoles.length; i++) {
      if (!ROLES.includes(req.body.userRoles[i])) {
        res.status(400).send({
          message: `Failed! Role ${req.body.userRoles[i]} does not exist!`
        });
        return;
      }
    }
  }

  next();
};

const verifySignUp = {
  checkDuplicateEmail,
  checkRolesExisted
};

module.exports = verifySignUp;