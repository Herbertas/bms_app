const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const companyInformationSchema = new Schema({

    companyInformationEmployeeId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Employee", 
        required: true, 
        unique: false
    },

    companyInformationCode: {
        type: String, 
        required: true, 
        trim: true,
        unique: true,
        minlength: 5
    },

    companyInformationVATCode: {
        type: String, 
        required: true, 
        trim: true,
        unique: true,
        minlength: 5
    },

    companyInformationName: {
        type: String, 
        required: true, 
        trim: true,
        unique: false,
        minlength: 3
    }, 

    companyInformationAdress: {
        type: String, 
        required: true, 
        trim: true,
        unique: false,
        minlength: 3
    }, 

    companyInformationBankAccount: {
        type: String, 
        required: true, 
        trim: true,
        unique: true,
        minlength: 20
    },

    companyInformationBankSwift: {
        type: String, 
        required: true, 
        trim: true,
        unique: false,
        minlength: 11
    }, 

    companyInformationBankIBAN: {
        type: String, 
        required: true, 
        trim: true,
        unique: false,
        minlength: 34, 
        maxlength: 34
    }, 

    companyInformationPhoneNumber: {
        type: String, 
        required: true, 
        trim: true,
        unique: false,
        minlength: 15
    }, 

    companyInformationEmail: {
        type: String, 
        required: true, 
        trim: true,
        unique: false,
        minlength: 5
    }

    
}, {
  timestamps: true,
});

const CompanyInformation = mongoose.model('companyInformation', companyInformationSchema);

module.exports = CompanyInformation;