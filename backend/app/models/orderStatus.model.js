const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const orderStatusSchema = new Schema({

    orderStatusName: {
        type: String,
        required: true, 
        unique: true,
    },
        
}, {
  timestamps: true,
});

const OrderStatus = mongoose.model('orderStatus', orderStatusSchema);

module.exports = OrderStatus;