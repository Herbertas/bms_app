const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const countrySchema = new Schema({

    countryName: {
        type: String, 
        required: true, 
        trim: false,
        unique: true,
        minlength: 3, 
    }
}, {
  timestamps: true,
});

const Country = mongoose.model('country', countrySchema);

module.exports = Country;