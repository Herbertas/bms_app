const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const customerPaymentStatusSchema = new Schema({

    customerPaymentStatusName: {
        type: String, 
        required: true, 
        trim: true,
        unique: true,
        minlength: 5
    },
    
}, {
  timestamps: true,
});

const CustomerPaymentStatus = mongoose.model('customerPaymentStatus', customerPaymentStatusSchema);

module.exports = CustomerPaymentStatus;