const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const customerSchema = new Schema({

    customerPaymentStatusId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "CustomerPaymentStatus", 
        required: true, 
        unique: false
    },

    countryId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Country", 
        required: true, 
        unique: false
    },

    customerCompanyCode: {
        type: String, 
        required: false, 
        trim: true,
        unique: true,
        minlength: 9
    },

    customerCompanyName: {
        type: String, 
        required: false, 
        trim: false,
        unique: false,
        minlength: 3
    }, 

    customerAdress: {
        type: String, 
        required: true, 
        trim: false,
        unique: false,
        minlength: 3
    }, 

    customerVATCode: {
        type: String, 
        required: false, 
        trim: true,
        unique: true,
        minlength: 9
    },

    customerContactPersonName: {
        type: String, 
        required: false, 
        trim: true,
        unique: false,
        minlength: 2
    }, 

    customerContactPersonPhoneNumber: {
        type: String, 
        required: true, 
        trim: true,
        unique: false,
        maxlength: 15
    }, 

    customerEmail: {
        type: String, 
        required: true, 
        trim: true,
        unique: false,
        minlength: 3
    }, 

    customerDeliveryAdress1: {
        type: String, 
        required: true, 
        trim: false,
        unique: false,
        minlength: 5
    }, 

    customerDeliveryAdress2: {
        type: String, 
        required: false, 
        trim: false,
        unique: false,
        minlength: 5
        
    }, 

    customerDeliveryPersonName: {
        type: String, 
        required: false, 
        trim: true,
        unique: false,
        minlength: 2
    }, 

    customerDeliveryPersonPhoneNumber: {
        type: String, 
        required: false, 
        trim: true,
        unique: false,
        maxlength: 15
    }, 

}, {
  timestamps: true,
});

const Customer = mongoose.model('customerInformation', customerSchema);

module.exports = Customer;