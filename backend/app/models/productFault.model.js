const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const productFaultSchema = new Schema({

    productFaultProductId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Product", 
        required: true, 
        unique: false
    },

    productFaultCustomerId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "CustomerInformation", 
        required: false,
        unique: false
    },

    productFaultEmployeeId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Employee", 
        required: true, 
        unique: false
    },
    
    productFaultProductFaultStatusId: {
        type: mongoose.Schema.Types.ObjectId, 
        ref: "ProductFaultStatus", 
        required: true,
        unique: false
    },

    productFaultDescription: {
        type: String, 
        required: true, 
        trim: false,
        unique: false,
        minlength: 3, 
        maxlength: 256
    }, 

}, {
  timestamps: true,
});

const ProductFault = mongoose.model('productFault', productFaultSchema);

module.exports = ProductFault;