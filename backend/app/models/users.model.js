const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const userSchema = new Schema({

    userFirstName: {
        type: String,
        required: true,
        unique: false,
        trim: true,
        minlength: 3
    },

    userLastName: {
        type: String,
        required: true, 
        unique: false, 
        minlength: 3
    },

    userEmail: {
        type: String, 
        required: true, 
        unique: true, 
        trim: true, 
        minlength: 3
    },

    userPassword: {
        type: String, 
        required: true, 
        unique: false, 
        trim: true, 
        minlength: 8
    },

    userRoles: [
        {
          type: mongoose.Schema.Types.ObjectId,
          ref: "Role"
        }
    ]

}, 
{
    timestamps: true,
})
;

const User = mongoose.model('User', userSchema);

module.exports = User;