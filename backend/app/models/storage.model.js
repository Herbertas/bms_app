const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const storageSchema = new Schema({

    storageAdress: {
        type: String, 
        required: true, 
        trim: false,
        unique: false,
        minlength: 3
    },
    
}, {
  timestamps: true,
});

const Storage = mongoose.model('storage', storageSchema);

module.exports = Storage;