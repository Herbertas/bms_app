const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const employeeSchema = new Schema({

    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User", 
        required: true, 
        unique: true
    },

    employeeHiredDate: {
        type: Date,
        required: false,
        unique: false,
        trim: true,
        minlength: 3
    },

    employeeFiredDate: {
        type: Date,
        required: false,
        unique: false,
        trim: true,
        minlength: 3
    },

    employeeAdress: {
        type: String,
        required: true,
        unique: false,
        trim: true,
        minlength: 3
    },

    employeePhoneNumber: {
        type: String, 
        required: true, 
        trim: true, 
        unique: false,
        maxlength: 15
    }


}, {
  timestamps: true,
});

const Employee = mongoose.model('employee', employeeSchema);

module.exports = Employee;