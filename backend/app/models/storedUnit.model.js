const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const storedUnitSchema = new Schema({

    productId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Product", 
        required: true, 
        unique: false
    },

    storageId: {
        type: mongoose.Schema.Types.ObjectId, 
        ref: "Storage", 
        required: true,
        unique: false
    },

    storedUnitProductQuantity: {
        type: Number, 
        required: true,
        default: 0
    }, 

    storedUnitProductOrderedQuantity: {
        type: Number, 
        required: true,
        default: 0
    }, 

    storedUnitProductManufacturingQuantity: {
        type: Number, 
        required: true, 
        default: 0
    }, 
    
    storedUnitProductDefectiveQuantity: {
        type: Number,
        required: true, 
        default: 0
    }
    
}, {
  timestamps: true,
});

const StoredUnit = mongoose.model('storedUnit', storedUnitSchema);

module.exports = StoredUnit;