const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const offerSchema = new Schema({

    customerId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "CustomerInformation",
        required: true, 
        unique: false
    },

    employeeId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Employee", 
        required: true, 
        unique: false
    },

    productId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Product", 
        required: true, 
        unique: false
    }, 

    offerQuantity: {
        type: Number, 
        required: true, 
    },

    offerPrice : {
        type: Number,
        required: true
    },

    offerValidFromDate: {
        type: Date,
        required: true,
        unique: false
    }, 

    offerValidTillDate: {
        type: Date, 
        required: true, 
        unique: false
    }
}, {
  timestamps: true,
});

const Offer = mongoose.model('offer', offerSchema);

module.exports = Offer;