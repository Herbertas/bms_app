const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const orderSchema = new Schema({

    orderStatusId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "OrderStatus", 
        required: true, 
        unique: false
    },

    customerId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "CustomerInformation", 
        required: true, 
        unique: false
    },

    employeeId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Employee",
        required: true, 
        unique: false
    },
    
    offerId: {
        type: mongoose.Schema.Types.ObjectId, 
        ref: "Offer", 
        required: true, 
        unique: true
    },

    salesVATInvoiceId: {
        type: mongoose.Schema.Types.ObjectId, 
        ref: "SalesVATInvoice", 
        required: true, 
        unique: true
    },

    orderDate: {
        type: Date, 
        required: true, 
        unique: false
    },

    orderTillDate: {
        type: Date, 
        required: true, 
        unique: false
    }


}, {
  timestamps: true,
});

const Order = mongoose.model('order', orderSchema);

module.exports = Order;