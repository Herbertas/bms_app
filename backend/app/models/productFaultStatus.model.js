const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const productFaultStatusSchema = new Schema({

    productFaultStatusName: {
        type: String, 
        required: true, 
        trim: true,
        unique: true,
        minlength: 5, 
        maxlength: 25
    }

}, {
  timestamps: false,
});

const ProductFaultStatus = mongoose.model('productFaultStatus', productFaultStatusSchema);

module.exports = ProductFaultStatus;