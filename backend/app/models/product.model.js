const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const productSchema = new Schema({

    productCode: {
        type: String, 
        required: true, 
        unique: true, 
        trim: true,
        minlength: 3
    },

    productName: {
        type: String, 
        required: true, 
        unique: false, 
        trim: true,
        minlength: 3
    },

    productDescription: {
        type: String, 
        required: false, 
        unique: false, 
        trim: false,
        minlength: 3, 
        maxlength: 256
    },

    productWeight: {
        type: Number, 
        required: true, 
        unique: false, 
    },

    productStandardPrice: {
        type: Number, 
        required: true, 
        unique: false, 
    }


}, {
  timestamps: true,
});

const Product = mongoose.model('product', productSchema);

module.exports = Product;