const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const salesVATInvoiceSchema = new Schema({

    customerId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "CustomerInformation", 
        required: true, 
        unique: false
    }, 

    employeeId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Employee", 
        required: true, 
        unique: false
    }, 

    consigmentNoteId: {
        type: mongoose.Schema.Types.ObjectId, 
        ref: "ConsigmentNote", 
        required: true, 
        unique: true
    }, 

    companyId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "CompanyInformation", 
        required: true, 
        unique: false
    }, 

    salesVATInvoiceVATDate: {
        type: Date, 
        required: true,
        unique: false
    }, 

    salesVATInvoicePayAmountBeforeVAT: {
        type: Number , 
        required: true, 
        unique: false
    }, 

    salesVATInvoiceAmountToPayBeforeVATInWords: {
        type: String, 
        required: true, 
        unique: false
    },

    salesVATInvoiceVATAmount: {
        type: Number, 
        required: true, 
        unique: false, 
        min: 0, 
        max: 100
    },

    salesVATInvoicePayAmountAfterVAT: {
        type: Number , 
        required: true, 
        unique: false
    }, 

    salesVATInvoiceAmountToPayAfterVATInWords: {
        type: String, 
        required: true, 
        unique: false
    },

    salesVATInvoiceAmountPayTill: {
        type: Date, 
        required: true, 
        unique: false
    }
}, {
  timestamps: true,
});

const SalesVATInvoice = mongoose.model('salesVATInvoice', salesVATInvoiceSchema);

module.exports = SalesVATInvoice;