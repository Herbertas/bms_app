const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const db = {};

db.mongoose = mongoose;

db.user = require("./users.model");
db.role = require("./role.model");
db.employee = require("./employee.model");
db.customer = require("./customers.model");


db.ROLES = ["user", "admin", "salesman", "manufacturer", "developer", "accountant", "customer_support_specialist", "warehousekeeper"];

module.exports = db;