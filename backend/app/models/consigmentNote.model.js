const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const consigmentNoteSchema = new Schema({

  consigmentNoteDate: {
    type: Date,
    required: true, 
    unique: false,
  }, 
  consigmentNoteWeight: {
    type: Number, 
    required: true, 
    unique: false,
  }, 
  consigmentNoteVolume: {
    type: Number, 
    required: true, 
    unique: false,
  }
    
}, {
  timestamps: true,
});

const ConsigmentNote = mongoose.model('consigmentNote', consigmentNoteSchema);

module.exports = ConsigmentNote;