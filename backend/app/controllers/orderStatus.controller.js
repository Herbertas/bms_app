const { mongoose } = require("../models");
const OrderStatus = require("../models/orderStatus.model");

exports.getAllOrderStatuses = (req, res) => {

    OrderStatus.find((err, orderStatuses) => {
        if(err)
        {
            console.log("Order statuses is not found in DB")
            res.status(500).send({message: err});
            return;
        }
        res.status(200).json(orderStatuses);
    });
};

exports.getOrderStatusById = (req, res) => {
    OrderStatus.findById(req.body.orderStatusId, (err, orderStatus) => {
        if(err) 
        {
            console.log("Order status is not found in DB")
            res.status(500).send({message: err});
            return;
        }

        res.status(200).json(orderStatus);
    });
};