const ProductFault = require("../models/productFault.model");
const { mongoose } = require("../models");


exports.getAllProductFaults = (req, res) => {
    ProductFault.find((err, productFaults) => {
        if(err)
        {
            console.log("Product Faults is not found in DB")
            res.status(500).send({message: err});
            return;
        }
        res.status(200).json(productFaults);
    })
};

exports.addProductFault = (req, res) => {
    console.log(req.body)
    const productFault = new ProductFault({
        productFaultProductId: mongoose.Types.ObjectId(req.body.productFaultProductId), 
        productFaultCustomerId: mongoose.Types.ObjectId(req.body.productFaultCustomerId), 
        productFaultEmployeeId: mongoose.Types.ObjectId(req.body.productFaultEmployeeId),
        productFaultProductFaultStatusId: mongoose.Types.ObjectId(req.body.productFaultProductFaultStatusId),
        productFaultDescription: req.body.productFaultDescription
    });
    
    productFault.save((err, response) => {
        if(err)
        {
            console.log("Product Fault is not added to DB")       
            res.status(500).send({ message: err });
            return;
        }
        res.status(200).send({message: "Product Fault added!"})
    })
}; 

exports.updateProductFault = (req, res) => {
    ProductFault.updateOne( {"_id": mongoose.Types.ObjectId(req.body.productFaultId)},
    { $set: {"productFaultProductId": req.body.productFaultProductId,
            "productFaultCustomerId" : req.body.productFaultCustomerId,
            "productFaultEmployeeId" : req.body.productFaultEmployeeId,
            "productFaultProductFaultStatusId" : req.body.productFaultProductFaultStatusId,
            "productFaultDescription" : req.body.productFaultDescription
            }
    },
    (err) => {
        if(err)
        {
            console.log("Product Fault is not found in DB")       
            res.status(500).send({ message: err });
            return; 
        }
        res.status(200).send({message: "Product Fault Updated!"})
    }
);
}