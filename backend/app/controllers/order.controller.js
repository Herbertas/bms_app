const Offer = require("../models/offer.model");
const Order = require("../models/order.model");
const ConsigmentNote = require ("../models/consigmentNote.model"); 
const SalesVATInvoice = require("../models/salesVATInvoice.model");
const { mongoose } = require("../models");


exports.getAllOrders = (req, res) => {
    Order.aggregate(
        [
            // Get Customer By ID
            {
                $lookup : {
                    from: 'customerinformations', 
                    localField: "customerId",
                    foreignField: '_id', 
                    as : 'customer'
                }
            },
            {
                $unwind: '$customer',
            },

            // Get Employee By ID
            {
                $lookup : {
                    from: 'employees', 
                    localField: "employeeId",
                    foreignField: '_id', 
                    as : 'employee'
                }
            }, 
            {
                $unwind: '$employee',
            },

            // Get Offer By ID
            {
                $lookup : {
                    from: 'offers', 
                    localField: "offerId",
                    foreignField: '_id', 
                    as : 'offer'
                }
            },
            {
                $unwind: '$offer',
            },

            // Get Sales VAT Invoice By ID
            {
                $lookup : {
                    from: 'salesvatinvoices', 
                    localField: "salesVATInvoiceId",
                    foreignField: '_id', 
                    as : 'salesVATInvoice'
                }
            },
            {
                $unwind: '$salesVATInvoice',
            },

        ]
    ).then(result => {
        res.status(200).json(result)
    })
}


exports.addNewOrderConsigmentNoteSalesVATInvoice = (req, res) => {

    console.log(req.body)
    
    const newConsigmentNote = new ConsigmentNote({
        consigmentNoteDate: req.body.consigmentNoteDate,
        consigmentNoteWeight: req.body.consigmentNoteWeight,
        consigmentNoteVolume: req.body.consigmentNoteVolume
    })

    newConsigmentNote.save((err, consigmentNote) => {
        if(err)
        {
            console.log("Consigment note is not added to DB")       
            res.status(500).send({ message: err });
            return;
        }
        const newSalesVATInvoice = new SalesVATInvoice({
            customerId: mongoose.Types.ObjectId(req.body.customerId),
            employeeId: mongoose.Types.ObjectId(req.body.employeeId),
            consigmentNoteId: mongoose.Types.ObjectId(consigmentNote._id),
            companyId: mongoose.Types.ObjectId(req.body.companyId),
            salesVATInvoiceVATDate: req.body.salesVATInvoiceVATDate,
            salesVATInvoicePayAmountBeforeVAT: Number(req.body.salesVATInvoicePayAmountBeforeVAT),
            salesVATInvoiceAmountToPayBeforeVATInWords: req.body.salesVATInvoiceAmountToPayBeforeVATInWords,
            salesVATInvoiceVATAmount: Number(req.body.salesVATInvoiceVATAmount),
            salesVATInvoicePayAmountAfterVAT: Number(req.body.salesVATInvoicePayAmountAfterVAT),
            salesVATInvoiceAmountToPayAfterVATInWords: String(req.body.salesVATInvoiceAmountToPayAfterVATInWords,),
            salesVATInvoiceAmountPayTill: req.body.salesVATInvoiceAmountPayTill
        })

        newSalesVATInvoice.save((err, salesVATInvoice) => {

            if(err)
            {
                console.log("Sales VAT Invoice is not added to DB")       
                res.status(500).send({ message: err });
                return;
            }

            const newOrder = new Order({
                orderStatusId: mongoose.Types.ObjectId(req.body.orderStatusId),
                customerId: mongoose.Types.ObjectId(req.body.customerId),
                employeeId: mongoose.Types.ObjectId(req.body.employeeId),
                offerId: mongoose.Types.ObjectId(req.body.offerId),
                salesVATInvoiceId: mongoose.Types.ObjectId(salesVATInvoice._id),
                orderDate: req.body.orderDate,
                orderTillDate: req.body.orderTillDate

            })

            newOrder.save((err, newOrder) => {
                if(err)
                {
                    console.log("Order is not added to DB")       
                    res.status(500).send({ message: err });
                    return;
                }
                res.status(200).send({message: "order added!"})
            })
        })
    });
}
