const Product = require("../models/product.model");
const { mongoose } = require("../models");


exports.getProducts = (req, res) => {
    Product.find((err, products) => {
        if(err)
        {
            console.log("Products is not found in DB")
            res.status(500).send({message: err});
            return;
        }
        res.status(200).json(products);
    })
}

exports.getProductById = (req, res) => {
    Product.findOne(req.body.productId, (err, product) => {
        if(err)
        {
            console.log("Product is not found in DB");
            res.status(500).send({message: err});
            return;
        }
        res.status(200).json(product);
    })
}

exports.addProduct = (req, res) => {
    const product = new Product ({
        productCode: req.body.productCode, 
        productName: req.body.productName, 
        productDescription: req.body.productDescription, 
        productWeight: req.body.productWeight,
        productStandardPrice: req.body.productStandardPrice
    }); 

    product.save((err, product) => {
        if(err)
        {
            console.log("Product is not added to DB")       
            res.status(500).send({ message: err });
            return;
        }
        res.status(200).send({message: "Product added!"})
    });
}

exports.updateProduct = (req, res) => {

    Product.updateOne( {"_id": mongoose.Types.ObjectId(req.body.productId)},
        { $set: {"productCode": req.body.productCode,
                "productName" : req.body.productName,
                "productDescription" : req.body.productDescription, 
                "productWeight" : req.body.productWeight,
                "productStandardPrice" : req.body.productStandardPrice
                }
        },
        (err) => {
            if(err)
            {
                console.log("Company is not found in DB")       
                res.status(500).send({ message: err });
                return; 
            }
            res.status(200).send({message: "Company Updated!"})
        }
    );
}
