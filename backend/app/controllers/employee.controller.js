const Employee = require("../models/employee.model");
const { mongoose } = require("../models");



exports.getEmployees = (req, res) => {
    Employee.aggregate(
        [
            {
                $lookup :
                {
                    from: 'users', 
                    localField : 'userId', 
                    foreignField : '_id', 
                    as : 'user'
                }
            },
            {
                $unwind: '$user',
            }

        ]
    ).then(result => {
        res.status(200).json(result)
    })
}

exports.getEmployeeByUserId = (req, res) => {
    Employee.findOne({userId : mongoose.Types.ObjectId(req.userId)}, (err, employee) => {
        if(err)
        {
            console.log("Employee is not found in DB");
            res.status(500).send({message: err});
            return;
        }
        if(!employee)
        {
            console.log("Employee is not found in DB");
            res.status(404).send({message: "Employee Is not Found"})
        }
        else
        {
            console.log("Employee is found in DB")
            res.status(200).json(employee);
        }
    
    });
}

exports.getEmployeeById = (req, res) => {
    Employee.findOne(req.body.employeeId, (err, employee) => {
        if(err)
        {
            console.log("Employee is not found in DB");
            res.status(500).send({message: err});
            return;
        }
        res.status(200).json(employee);
    })
}

exports.addEmployee = (req, res) => {
    const employee = new Employee({
        userId: req.body.userId,
        employeeHiredDate: req.body.employeeHiredDate, 
        employeeFiredDate: req.body.employeeFiredDate, 
        employeeAdress: req.body.employeeAdress, 
        employeePhoneNumber: req.body.employeePhoneNumber

    });
    employee.save((err, employee) => {
        if(err)
        {
            console.log("Employee is not added to DB")       
            res.status(500).send({ message: err });
            return;
        }

        res.status(200).send({message: "Employee added!"})
    });
}

exports.updateEmployee = (req, res) => {

    Employee.updateOne( {"_id": mongoose.Types.ObjectId(req.body.employeeId)},
        { $set: {"userId": req.body.userId,
                "employeeHiredDate" : req.body.employeeHiredDate, 
                "employeeFiredDate" : (req.body.employeeFiredDate =! null) ? req.body.employeeFiredDate : "",
                "employeeAdress" : req.body.employeeAdress, 
                "employeePhoneNumber" : req.body.employeePhoneNumber,
                }
        },
        (err) => {
        if(err)
        {
            console.log("Employee is not found in DB")       
            res.status(500).send({ message: err });
            return; 
        }
        res.status(200).send({message: "Employee Updated!"})

    });
}