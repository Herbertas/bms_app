const Country = require("../models/country.model");

exports.addCountry = (req, res) => {

    const country = new Country ({
        countryName: req.body.countryName, 
    });

    country.save((err, country) => {
        if(err)
        {
            console.log("Country is not added to DB")       
            res.status(500).send({ message: err });
            return;
        }

        res.status(200).send({message: "Country added!"})
    });
};

exports.getCountries = (req, res) => {
    Country.find((err, countries) => {
        if(err)
        {
            console.log("Countries is not found in DB")
            res.status(500).send({message: err});
            return;
        }
        
        res.status(200).json(countries);
    });
};

exports.getCountryById = (req, res) => {
    Country.findById(req.body.countryId, (err, country) => {
        if(err)
        {
            console.log("Country is not found in DB")       
            res.status(500).send({ message: err });
            return;
        }
        if(!country)
        {
            console.log("Country is not found in DB")       
            return res.status(404).send({ message: "Country Not found." });
        }

        res.status(200).json(country)
    })
}