const Role = require("../models/role.model");


exports.getRoles = (req, res) => {
    Role.find((err, roles) => {
        if(err)
        {
            console.log("Roles is not found in DB")
            res.status(500).send({message: err});
            return;
        }
        
        res.status(200).json(roles);
    });
};
