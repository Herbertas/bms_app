const config = require("../config/auth.config");
const db = require("../models");
const User = db.user;
const Role = db.role;
const Employee = db.employee;

var jwt = require("jsonwebtoken");
var bcrypt = require("bcrypt");
const { mongoose } = require("../models");


exports.signup = (req, res) => {

  const salt = bcrypt.genSaltSync(10);
  const hash = bcrypt.hashSync(req.body.userPassword, salt);

  const user = new User ({
    userFirstName: req.body.userFirstName, 
    userLastName: req.body.userLastName, 
    userEmail: req.body.userEmail, 
    userPassword: hash,
  })

  console.log(user)

  user.save((err, user) => {
    if (err) {
        console.log("User is not added to DB")       
        res.status(500).send({ message: err });
        return;
    }

    if (req.body.userRoles) {
      Role.find(
        {
          roleName: { $in: req.body.userRoles }
        },
        (err, userRoles) => {
          if (err) {
            console.log("Role is not found in DB")
            res.status(500).send({ message: err });
            return;
          }
          console.log(userRoles);
          user.userRoles = userRoles.map(userRoles => userRoles._id);
          user.save(err => {
            if (err) {
                console.log("b")
                res.status(500).send({ message: err });
                return;
            }
          });
        }
      );
    } else {
      Role.findOne({ roleName: "user" }, (err, userRoles) => {
        if (err) {
            console.log("User role is not found in DB");
            res.status(500).send({ message: err });
            return;
        }

        user.userRoles = [userRoles._id];
        user.save(err => {
            if (err) {
                console.log("User Role ID is not added to DB");
                res.status(500).send({ message: err });
                return;
            }
        });
      });
    }

    if(req.body.employeeHiredDate && req.body.employeeAdress && req.body.employeePhoneNumber)
    {
      const employee = new Employee({
        userId: mongoose.Types.ObjectId(user._id),
        employeeHiredDate: (req.body.employeeHiredDate), 
        employeeFiredDate: (req.body.employeeFiredDate),
        employeeAdress: req.body.employeeAdress, 
        employeePhoneNumber: req.body.employeePhoneNumber
      })

      console.log(employee)

      employee.save((err, employee) => {
        if(err)
        {
          console.log("Employee is not added to DB")       
          res.status(500).send({ message: err });
          return;
        }
        res.status(200).send({ message: "User and Employee was registered successfully!" });

      })
    }
    else
    {
      console.log("Employee is not added to DB")
      res.status(500).send({ message: err });

    }
  });
};

exports.signin = (req, res) => {
  User.findOne({
    userEmail: req.body.userEmail
  })
    .populate("userRoles", "-__v")
    .exec((err, user) => {
      if (err) {
        res.status(500).send({ message: err });
        return;
      }

      if (!user) {
        return res.status(404).send({ message: "User Not found." });
      }

      var passwordIsValid = bcrypt.compareSync(
        req.body.userPassword,
        user.userPassword
      );

      if (!passwordIsValid) {
        return res.status(401).send({
          userAccessToken: null,
          message: "Invalid Password!"
        });
      }

      var token = jwt.sign({ id: user.id }, config.secret, {
        expiresIn: 3600 // 24 hours
      });

      console.log(token)

      var authorities = [];
      console.log(user)
      for (let i = 0; i < user.userRoles.length; i++) {
        authorities.push("ROLE_" + user.userRoles[i].roleName.toUpperCase());
      }
      res.status(200).send({
        id: user._id,
        userEmail: user.userEmail,
        userRoles: authorities,
        userAccessToken: token
      });
    });
};
