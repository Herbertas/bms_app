const User = require("../models/users.model");



exports.getAllUsers = (req, res) => {
    User.find()
    .then(users => res.status(200).json(users))
    .catch(err => res.status(400).json('Error: ' + err));
};

exports.allAccess = (req, res) => {
    res.status(200).send("Public Content.");
};

exports.userBoard = (req, res) => {
    res.status(200).send("User Content.");
};

exports.adminBoard = (req, res) => {
    res.status(200).send("Admin Content.");
};

exports.salesmanBoard = (req, res) => {
    res.status(200).send("Salesman Content.");
};

exports.developerBoard = (req, res) => {
    res.status(200).send("Developer Content.");
};
    
exports.manufacturerBoard = (req, res) => {
    res.status(200).send("Manufacturer Content.");
};