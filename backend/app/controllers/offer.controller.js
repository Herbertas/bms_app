const Offer = require("../models/offer.model");
const { mongoose } = require("../models");


exports.getAllOffers = (req, res) => {
    Offer.aggregate(
        [
            {
                $lookup : {
                    from: 'products', 
                    localField: "productId",
                    foreignField: '_id', 
                    as : 'product'
                }
            },
            {
                $unwind: '$product',
            },
            {
                $lookup : {
                    from: 'employees', 
                    localField: "employeeId",
                    foreignField: '_id', 
                    as : 'employee'
                }
            }, 
            {
                $unwind: '$employee',
            },
            {
                $lookup : {
                    from: 'customerinformations', 
                    localField: "customerId",
                    foreignField: '_id', 
                    as : 'customer'
                }
            },
            {
                $unwind: '$customer',
            },

        ]
    ).then(result => {
        res.status(200).json(result)
    })
}

exports.getOfferByCustomerId = (req, res) => {
    Offer.aggregate(
        [
            {
                $lookup : {
                    from: 'products', 
                    localField: "productId",
                    foreignField: '_id', 
                    as : 'product'
                }
            },
            {
                $unwind: '$product',
            },
            {
                $match:{
                    $and:[{customerId : mongoose.Types.ObjectId(req.query.customerId)}]
                }
            }


        ]
    ).then(result => {
        res.status(200).json(result)
    })
}

exports.addNewOffer = (req, res) => {

    console.log(req.body)
    const offer = new Offer ({
        customerId: mongoose.Types.ObjectId(req.body.customerId), 
        employeeId: mongoose.Types.ObjectId(req.body.employeeId),
        productId: mongoose.Types.ObjectId(req.body.productId),
        offerQuantity: req.body.offerQuantity, 
        offerValidFromDate: req.body.offerValidFromDate, 
        offerValidTillDate: req.body.offerValidTillDate,
        offerPrice: req.body.offerPrice
    });
    

    offer.save((err, offer) => {
        if(err)
        {
            console.log("offer is not added to DB")       
            res.status(500).send({ message: err });
            return;
        }
        res.status(200).send({message: "offer added!"})
    });
}
