const { mongoose } = require("../models");
const Company = require("../models/company.model");

exports.addCompany = (req, res) => {
    
    const company = new Company ({
        companyInformationEmployeeId: mongoose.Types.ObjectId(req.employeeId), 
        companyInformationCode: req.body.companyInformationCode, 
        companyInformationVATCode: req.body.companyInformationVATCode, 
        companyInformationName: req.body.companyInformationName,
        companyInformationAdress: req.body.companyInformationAdress, 
        companyInformationBankAccount: req.body.companyInformationBankAccount, 
        companyInformationBankSwift: req.body.companyInformationBankSwift, 
        companyInformationBankIBAN: req.body.companyInformationBankIBAN,
        companyInformationPhoneNumber: req.body.companyInformationPhoneNumber,
        companyInformationEmail: req.body.companyInformationEmail
    });

    company.save((err, company) => {
        if(err)
        {
            console.log("Company is not added to DB")       
            res.status(500).send({ message: err });
            return;
        }

        res.status(200).send({message: "Company added!"})
    });
};

exports.getCompanies = (req, res) => {
    Company.find((err, companies) => {
        if(err)
        {
            console.log("Companies is not found in DB")
            res.status(500).send({message: err});
            return;
        }
        
        res.status(200).json(companies);
    });
};

exports.getCompanyById = (req, res) => {
    Company.findById(req.body.companyId, (err, company) => {
        if(err)
        {
            console.log("Company is not found in DB")       
            res.status(500).send({ message: err });
            return;
        }
        if(!company)
        {
            console.log("Company is not found in DB")       
            return res.status(404).send({ message: "Company Not found." });
        }

        res.status(200).json(company)
    })
}

exports.updateCompanyInformation = (req, res) => {

    Company.updateOne( {"_id": mongoose.Types.ObjectId(req.body.companyId)},
        { $set: {"companyInformationEmployeeId": req.body.companyInformationEmployeeId,
                "companyInformationCode" : req.body.companyInformationCode,
                "companyInformationVATCode" : req.body.companyInformationVATCode, 
                "companyInformationName" : req.body.companyInformationName,
                "companyInformationAdress" : req.body.companyInformationAdress, 
                "companyInformationBankAccount" : req.body.companyInformationBankAccount,
                "companyInformationBankSwift" : req.body.companyInformationBankSwift, 
                "companyInformationBankIBAN" : req.body.companyInformationBankIBAN, 
                "companyInformationPhoneNumber" : req.body.companyInformationPhoneNumber,
                "ompanyInformationEmail" :  req.body.companyInformationEmail
                }
        },
        (err) => {
        if(err)
        {
            console.log("Company is not found in DB")       
            res.status(500).send({ message: err });
            return; 
        }
        res.status(200).send({message: "Company Updated!"})

    });
}