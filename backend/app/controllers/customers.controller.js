const Customer = require("../models/customers.model");
const Country = require("../models/country.model")
const { mongoose } = require("../models");

exports.getCustomers = (req, res) => {

    Customer.aggregate(
        [
            {
                $lookup : {
                    from: 'countries', 
                    localField : 'countryId', 
                    foreignField : '_id', 
                    as : 'country'
                }
            }, 
            {
                $lookup : {
                    from: 'customerpaymentstatuses',
                    localField: 'customerPaymentStatusId',
                    foreignField: '_id',
                    as: 'customerPaymentStatus'
                }
            }, 
            {
                $unwind: '$country',
            },
            {
                $unwind: '$customerPaymentStatus',
            }
        ], 
    ).then(result => {
        res.status(200).json(result);
    })
}

exports.getCustomerById = (req, res) => {
    Customer.findOne(req.body.customerId, (err, customer) => {
        if(err)
        {
            console.log("Customer is not found in DB");
            res.status(500).send({message: err});
            return;
        }
        res.status(200).json(customer);
    })
}

exports.addCustomer = (req, res) => {

    console.log("Labas")
    console.log(req.body.customerPaymentStatusId); 
    console.log(req.body.countryId); 
    console.log("Ate")

    const customer = new Customer({
        customerPaymentStatusId: mongoose.Types.ObjectId(req.body.customerPaymentStatusId),
        countryId: mongoose.Types.ObjectId(req.body.countryId), 
        customerCompanyCode: req.body.customerCompanyCode, 
        customerCompanyName: req.body.customerCompanyName, 
        customerAdress: req.body.customerAdress, 
        customerVATCode: req.body.customerVATCode, 
        customerContactPersonName: req.body.customerContactPersonName, 
        customerContactPersonPhoneNumber: req.body.customerContactPersonPhoneNumber, 
        customerEmail: req.body.customerEmail, 
        customerDeliveryAdress1: req.body.customerDeliveryAdress1, 
        customerDeliveryPersonName: req.body.customerDeliveryPersonName, 
        customerDeliveryPersonPhoneNumber: req.body.customerDeliveryPersonPhoneNumber

    });
    customer.save((err, customer) => {
        if(err)
        {
            console.log("Customer is not added to DB")   
            console.log(err);    
            res.status(500).send({ message: err });
            return;
        }
        console.log("Saved")
        res.status(200).send({message: "Country added!"})
    });
}

exports.updateCustomer = (req, res) => {

    Customer.updateOne( {"_id": mongoose.Types.ObjectId(req.body.customerId)},
        { $set: {"customerPaymentStatusId": req.body.customerPaymentStatusId,
                "countryId" : req.body.countryId,
                "customerCompanyCode" : req.body.customerCompanyCode, 
                "customerCompanyName" : req.body.customerCompanyName,
                "customerAdress" : req.body.customerAdress, 
                "customerVATCode" : req.body.customerVATCode,
                "customerContactPersonName" : req.body.customerContactPersonName, 
                "customerContactPersonPhoneNumber" : req.body.customerContactPersonPhoneNumber, 
                "customerEmail" : req.body.customerEmail,
                "customerDeliveryAdress1" :  req.body.customerDeliveryAdress1, 
                "customerDeliveryAdress2" : req.body.customerDeliveryAdress2, 
                "customerDeliveryPersonName" : req.body.customerDeliveryPersonName, 
                "customerDeliveryPersonPhoneNumber" : req.body.customerDeliveryPersonPhoneNumber
                }
        },
        (err) => {
        if(err)
        {
            console.log("Customer is not found in DB")       
            res.status(500).send({ message: err });
            return; 
        }
        res.status(200).send({message: "Customer Updated!"})

    });
}
