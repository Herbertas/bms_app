const CustomerPaymentStatus = require("../models/customerPaymentStatus.model");

exports.getCustomerPaymentStatuses = (req, res) => {

    CustomerPaymentStatus.find((err, customerPaymentStatuses) => {
        if(err)
        {
            console.log("Customer payment statuses is not found in DB")
            res.status(500).send({message: err});
            return;
        }
        res.status(200).json(customerPaymentStatuses);
    });
};

exports.getCustomerPaymentStatusById = (req, res) => {
    CustomerPaymentStatus.findOne(req.body.customerPaymentStatusId, (err, customerPaymentStatus) => {
        if(err) 
        {
            console.log("Customer payment status is not found in DB")
            res.status(500).send({message: err});
            return;
        }

        res.status(200).json(customerPaymentStatus);
    });
};