const ProductFaultStatus = require("../models/productFaultStatus.model");
const { mongoose } = require("../models");


exports.getAllProductFaultStatuses = (req, res) => {
    ProductFaultStatus.find((err, productFaultStatuses) => {
        if(err)
        {
            console.log("Product Faults Statuses is not found in DB")
            res.status(500).send({message: err});
            return;
        }
        res.status(200).json(productFaultStatuses);
    })
}; 
