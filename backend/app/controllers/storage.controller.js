const Storage = require("../models/storage.model");
const { mongoose } = require("../models");

exports.getAllStorages = (req, res) => {
    Storage.find((err, storages) => {
        if(err)
        {
            console.log("Storages is not found in DB")
            res.status(500).send({message: err});
            return;
        }
        
        res.status(200).json(storages);
    });
}