const StoredUnit = require("../models/storedUnit.model");
const { mongoose } = require("../models");
const Product = require("../models/product.model");


exports.addNewStoredUnit = (req, res,) => {
    const newStoredUnit = new StoredUnit({
        productId: mongoose.Types.ObjectId(req.body.productId),
        storageId: mongoose.Types.ObjectId(req.body.storageId), 
        storedUnitProductQuantity: req.body.storedUnitProductQuantity,
        storedUnitProductOrderedQuantity: req.body.storedUnitProductOrderedQuantity,
        storedUnitProductManufacturingQuantity: req.body.storedUnitProductManufacturingQuantity,
        storedUnitProductDefectiveQuantity: req.body.storedUnitProductDefectiveQuantity
    });

    newStoredUnit.save((err, storedUnit) => {
        if(err)
        {
            console.log("Stored Unit is not added to DB")       
            res.status(500).send({ message: err });
            return;
        }

        res.status(200).send({message: "Stored Unit added!"})
    })
}

exports.getAllStoredUnitsByStorageId = (req, res) => {
    console.log(req.query.storageId)
    StoredUnit.aggregate(
        [
            {
                $lookup : {
                    from: 'products', 
                    localField: "productId",
                    foreignField: '_id', 
                    as : 'product'
                }
            },
            {
                $unwind: '$product',
            },
            // define some conditions here 
            {
                $match:{
                    $and:[{storageId : mongoose.Types.ObjectId(req.query.storageId)}]
                }
            }
        ]
    ).then(result => {
        res.status(200).json(result)
    })
}