const { mongoose } = require("../models");
const ConsigmentNote = require("../models/consigmentNote.model");

exports.getAllConsigmentNotes = (req, res) => {

    ConsigmentNote.find((err, consigmentNotes) => {
        if(err)
        {
            console.log("Consigment notes is not found in DB")
            res.status(500).send({message: err});
            return;
        }
        res.status(200).json(consigmentNotes);
    });
};

exports.getConsigmentNoteById = (req, res) => {
    ConsigmentNote.findById(req.body.consigmentNoteId, (err, consigmentNote) => {
        if(err) 
        {
            console.log("Consigment Note is not found in DB")
            res.status(500).send({message: err});
            return;
        }

        res.status(200).json(consigmentNote);
    });
};