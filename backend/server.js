const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const app = express();
const db = require("./app/models");
const Role = db.role;
const User = db.user; 
const Employee = db.employee;
var bcrypt = require("bcrypt");

require('dotenv').config();

const port = process.env.PORT || 5000;

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

const uri = process.env.ATLAS_URI;
mongoose.connect(uri, { useNewUrlParser: true, useCreateIndex: true }
);
const connection = mongoose.connection;
connection.once('open', () => {
  initial();
  console.log("MongoDB database connection established successfully");
})

function initial() {
  Role.estimatedDocumentCount((err, count) => {
    if (!err && count === 0) {
      new Role({
        roleName: "user",
        roleLabel:"Naudotojas"
      }).save(err => {
        if (err) {
          console.log("error", err);
        }

        console.log("added 'user' to roles collection");
      });

      new Role({
        roleName: "salesman",
        roleLabel:"Pardavimų vadybininkas"
      }).save(err => {
        if (err) {
          console.log("error", err);
        }

        console.log("added 'salesman' to roles collection");
      });

      new Role({
        roleName: "admin",
        roleLabel: "Administratorius"
      }).save(err => {
        if (err) {
          console.log("error", err);
        }

        console.log("added 'admin' to roles collection");
      });

      new Role({
        roleName: "manufacturer",
        roleLabel: "Gamybos darbuotojas"
      }).save(err => {
        if (err) {
          console.log("error", err);
        }

        console.log("added 'manufacturer' to roles collection");
      });

      new Role({
        roleName: "developer",
        roleLabel: "R&D darbuotojas"
      }).save(err => {
        if (err) {
          console.log("error", err);
        }

        console.log("added 'developer' to roles collection");
      });
    }
  });

  User.estimatedDocumentCount((err, count) => {
    if (!err && count === 0) 
    {
      const salt = bcrypt.genSaltSync(10);
      const hash = bcrypt.hashSync("First User", salt);


      const user = new User ({
        userFirstName: "First User", 
        userLastName: "First User", 
        userEmail: "FirstUser@email.com", 
        userPassword: hash,
      })

      console.log(user)

      user.save((err, user) => {
        if (err) {
            console.log("User is not added to DB")       
            return;
        }

        Role.find(
          {
            roleName: "admin"
          },
          (err, userRoles) => {
            if (err) {
              console.log("Role is not found in DB")
              return;
            }
            console.log(userRoles);
            user.userRoles = userRoles.map(userRoles => userRoles._id);
            user.save(err => {
              if (err) {
                  console.log("b")
                  return;
              }
            });
          }
        );

        const employee = new Employee({
          userId: mongoose.Types.ObjectId(user._id),
          employeeHiredDate: new Date(), 
          employeeFiredDate: new Date(),
          employeeAdress: "Gatve 2", 
          employeePhoneNumber: "868623450"
        })

        console.log(employee)

        employee.save((err, employee) => {
          if(err)
          {
            console.log("Employee is not added to DB")       
            return;
          }
        })

      });
    }
  });
}

require("./app/routes/auth.routes")(app);
require("./app/routes/company.routes")(app);
require("./app/routes/country.routes")(app);
require("./app/routes/customerPaymentStatus.routes")(app); 
require("./app/routes/customers.routes")(app); 
require("./app/routes/employee.routes")(app); 
// require("./app/routes/employeePosition.routes")(app);
require("./app/routes/product.routes")(app); 
require("./app/routes/productFault.routes")(app); 
require("./app/routes/storedUnit.routes")(app);
require("./app/routes/storage.routes")(app);
require("./app/routes/user.routes")(app);
require("./app/routes/roles.routes")(app);
require("./app/routes/productFaultStatus.routes")(app);
require("./app/routes/offer.routes")(app);
require("./app/routes/orderStatus.routes")(app);
require("./app/routes/consigmentNote.routes")(app);
require("./app/routes/order.routes")(app);



app.listen(port, () => {

});
